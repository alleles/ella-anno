SHELL ?= /bin/bash
PAGER ?= less

_IGNORE_VARS =

MAKEFILE_DIR := $(dir $(abspath $(firstword $(MAKEFILE_LIST))))
MAKEFILE_UTILS := $(MAKEFILE_DIR)/Makefile.utils
include $(MAKEFILE_UTILS)

BRANCH ?= $(shell git rev-parse --abbrev-ref HEAD)
COMMIT_HASH = $(shell git rev-parse --short=8 HEAD)
COMMIT_DATE = $(shell git log -1 --pretty=format:'%cI')
# sed is important to strip out any creds (i.e., CI token) from repo URL
REPO_URL = $(shell git remote get-url origin | sed -e 's/.*\?@//; s/:/\//g')
API_PORT ?= 6000-6100
TARGETS_FOLDER ?= $(PWD)/anno-targets
TARGETS_OUT ?= $(PWD)/anno-targets-out
SAMPLE_REPO ?= $(PWD)/sample-repo
ANNO_DATA ?= $(PWD)/data
ANNO_RAWDATA ?= $(PWD)/rawdata

# set USE_REGISTRY to use the gitlab registry image names
ifeq ($(USE_REGISTRY),)
BASE_IMAGE = local/ella-anno
DOCKER_PREFIX = docker-daemon://
else
BASE_IMAGE = registry.gitlab.com/alleles/ella-anno
DOCKER_PREFIX = docker://
endif

# Docker/Singularity labels should follow OCI standard
# ref: https://github.com/opencontainers/image-spec/blob/main/annotations.md
OCI_BASE_LABEL = org.opencontainers.image
ANNO_BASE_LABEL = io.ousamg.anno
override BUILD_OPTS += --build-arg BUILDKIT_INLINE_CACHE=1 \
	--label $(OCI_BASE_LABEL).url=https://allel.es/anno-docs/ \
	--label $(OCI_BASE_LABEL).revision="$(COMMIT_HASH)" \
	--label $(ANNO_BASE_LABEL).git.url="$(REPO_URL)" \
	--label $(ANNO_BASE_LABEL).git.commit_hash="$(COMMIT_HASH)" \
	--label $(ANNO_BASE_LABEL).git.commit_date="$(COMMIT_DATE)" \
	--label $(ANNO_BASE_LABEL).git.branch="$(BRANCH)"

# Use release/annotated git tag if available, otherwise branch name
RELEASE_TAG ?= $(shell git tag -l --points-at HEAD)
ifneq ($(RELEASE_TAG),)
override BUILD_OPTS += --label $(OCI_BASE_LABEL).version=$(RELEASE_TAG)
PROD_TAG = $(RELEASE_TAG)
BUILDER_TAG = builder-$(RELEASE_TAG)
else
PROD_TAG = $(BRANCH)
BUILDER_TAG = builder-$(BRANCH)
endif
DEVC_TAG = $(PROD_TAG)-devcontainer

# Combine image / tags from above for docker slug, or use provided
IMAGE_NAME ?= $(BASE_IMAGE):$(PROD_TAG)
ANNOBUILDER_IMAGE_NAME ?= $(BASE_IMAGE):$(BUILDER_TAG)
DEVC_IMAGE_NAME ?= $(BASE_IMAGE):$(DEVC_TAG)
# default names when running prod or builder docker images
CONTAINER_NAME ?= anno-$(PROD_TAG)-$(USER)
ANNOBUILDER_CONTAINER_NAME ?= anno-$(BUILDER_TAG)-$(USER)

# Singularity naming should mirror Docker, but ignores builder
SINGULARITY_IMAGE_NAME ?= anno-$(PROD_TAG).sif
SINGULARITY_SANDBOX_PATH = anno-$(PROD_TAG)/
SINGULARITY_INSTANCE_NAME ?= $(CONTAINER_NAME)
SINGULARITY_PG_UTA_DATA = $(PWD)/pg_uta
SINGULARITY_LOG_DIR = $(HOME)/.singularity/instances/logs/$(shell hostname)/$(USER)
SINGULARITY_LOG_STDERR = $(SINGULARITY_LOG_DIR)/$(SINGULARITY_INSTANCE_NAME).err
SINGULARITY_LOG_STDOUT = $(SINGULARITY_LOG_DIR)/$(SINGULARITY_INSTANCE_NAME).out
SINGULARITY_ANNO_LOGS := $(PWD)/logs
# Large temporary storage is needed for gnomAD data generation. Set this to somewhere with at least
# 50GB of space if that amount is not available on /tmp's partition
TMP_DIR ?= /tmp

# Get user/group ID to generate/download data sets as
_UID ?= $(shell id -u)
_GID ?= $(shell id -g)
UID_GID ?= $(shell id -u):$(shell id -g)
override BUILD_OPTS += --build-arg ANNO_USER_UID=${_UID} --build-arg ANNO_USER_GID=${_GID}

# Use docker buildkit for faster builds
DOCKER_BUILDKIT ?= 1

# If DB_CREDS is set, source its environment variables into the docker container
ifneq ($(DB_CREDS),)
ifeq ($(shell realpath $(DB_CREDS)),)
$(error File DB_CREDS="$(DB_CREDS)" does not exist)
else
override ANNOBUILDER_OPTS += --env-file $(shell realpath $(DB_CREDS))
endif
endif

# If SPACES_CONFIG is set, the file should be mounted into the docker container
ifneq ($(SPACES_CONFIG),)
ifeq ($(shell realpath $(SPACES_CONFIG)),)
$(error File SPACES_CONFIG="$(SPACES_CONFIG)" does not exist)
else
override ANNOBUILDER_OPTS += -v $(shell realpath $(SPACES_CONFIG)):/anno/ops/spaces_config.json
endif
endif

## ---------------------------------------------
## Help / Debugging
##  Other vars: VAR_ORIGIN, FILTER_VARS
## ---------------------------------------------
.PHONY: vars local-vars help

all: help

help: ## print this help and exit
	$(print-help)

local-vars: ## print out vars set by command line and in the Makefile
	$(disable-origins)
	$(list-vars)
	@true

vars: ## print out variables available in the Makefile and the origin of their value
	$(list-vars)
	@true

## -------------------------------------------------------------------------------------------------
## General Development
##   Docker commands run in container $CONTAINER_NAME based on $IMAGE_NAME
##   Other vars: BUILD_OPTS, API_PORT, ANNO_DATA
## -------------------------------------------------------------------------------------------------
.PHONY: any build build-debug build-devcontainer build-prod build-release pull pull-prod
.PHONY: run kill logs restart shell stop test test-lint test-ops localclean

# Used to override the default CONTAINER_NAME to anything matching /anno-.*-$(USER)/.
# Must be placed _before_ the desired action, e.g., to stop and rm the first matching container:
# > make any kill
any:
	$(eval CONTAINER_NAME = $(shell docker ps | awk '/anno-.*-$(USER)/ {print $$NF}'))
	@true

build: build-prod ## build Docker image of 'prod' target named IMAGE_NAME

build-debug: ## like build, but disabling buildkit to expose intermediate layers
	DOCKER_BUILDKIT= docker build -t $(IMAGE_NAME) $(BUILD_OPTS) --target prod .

build-devcontainer: ## build Docker image of 'dev' target named DEVC_IMAGE_NAME
	docker build -t $(DEVC_IMAGE_NAME) $(BUILD_OPTS) --target dev .

build-prod:
	docker build -t $(IMAGE_NAME) $(BUILD_OPTS) --target prod .

build-release:
	git archive --format tar  $(if $(CI_COMMIT_SHA),$(CI_COMMIT_SHA),$(PROD_TAG)) \
		| docker build -t $(IMAGE_NAME) $(BUILD_OPTS) --target prod -

pull: pull-prod ## pull Docker image named IMAGE_NAME from registry, requires USE_REGISTRY

pull-prod:
	$(call check_defined, USE_REGISTRY, You must set USE_REGISTRY to pull remote images)
	docker pull $(IMAGE_NAME)

run: ## run container CONTAINER_NAME based on IMAGE_NAME (additional variables: API_PORT, ANNO_OPTS)
	docker run -d \
	-e TARGET_DATA=/target_data \
	-p $(API_PORT):6000 \
	-v $(ANNO_DATA):/anno/data \
	-v $(SAMPLE_REPO):/samples \
	-v $(TARGETS_FOLDER):/targets \
	-v $(TARGETS_OUT):/targets-out \
	--name $(CONTAINER_NAME) \
	--restart=always \
	$(ANNO_OPTS) \
	$(IMAGE_NAME)

kill: ## forcibly stop and remove CONTAINER_NAME
	docker rm -f -v $(CONTAINER_NAME) || :

logs: ## tail logs from CONTAINER_NAME
	docker logs -f $(CONTAINER_NAME)

restart: stop ## restart CONTAINER_NAME
	docker start $(CONTAINER_NAME) || :

shell: ## get a bash shell in CONTAINER_NAME
	docker exec -it $(CONTAINER_NAME) bash

stop: ## stop CONTAINER_NAME
	docker stop $(CONTAINER_NAME) || :

test: ## run tests in container CONTAINER_NAME based on IMAGE_NAME (requires all current data)
	docker run -t \
	-v $(ANNO_DATA):/anno/data -v /pg_uta \
	--name $(CONTAINER_NAME)-test --rm \
	$(IMAGE_NAME) /anno/ops/run_tests.sh

test-lint: ## run shellcheck/shfmt linting on all bash scripts
	$(eval override ANNOBUILDER_OPTS += -v $(PWD)/.devcontainer:/anno/.devcontainer)
	docker run -u "$(UID_GID)" $(TERM_OPTS) -v "$(PWD):/anno" $(DEVC_IMAGE_NAME) pipenv run linter -v

test-ops: ## run the ops tests in IMAGE_NAME -- \
## WARNING: will overwrite data dir if attached and running manually
	docker run -t \
	--name $(CONTAINER_NAME)-ops-test --rm \
	$(IMAGE_NAME) /anno/ops/run_ops_tests.sh

localclean: ## remove data, rawdata, thirdparty dirs and docker volumes
	rm -rf thirdparty/ data/ rawdata/
	-docker volume rm ella-anno-exts

## -------------------------------------------------------------------------------------------------
## Annotation builder: generate / download processed data sets for Anno
##   Docker commands run in container $ANNOBUILDER_CONTAINER_NAME based on $ANNOBUILDER_IMAGE_NAME
##   Other variables: DB_CREDS, PKG_NAME, RUN_CMD_ARGS, ANNO_DATA, ANNO_RAWDATA
## -------------------------------------------------------------------------------------------------
.PHONY: build-all build-annobuilder pull-all pull-annobuilder
.PHONY: annobuilder-exec annobuilder-run annobuilder-shell
.PHONY: download-data download-package upload-data upload-package generate-data generate-package
.PHONY: verify-digital-ocean install-thirdparty install-package tar-data untar-data
.PHONY: pipenv-update pipenv-check

ifeq ($(CI),)
# Use interactive/tty if running locally
BASH_I = -i
TERM_OPTS := -it
else
BASH_I :=
TERM_OPTS :=
endif

# Ensure $ANNO_DATA exists so it's not created as root owned by docker.
# If FASTA is set, resolve its path in case of symlink, check it exists, add to docker mounts and
# set env var in container. Use override to prevent FASTA_PATH/FASTA_EXISTS being set by user/env
# NOTE: multiple if statements and evals used for clarity, can also condensed to a single $(if ...)
# TODO: add _ to FASTA_PATH, FASTA_EXISTS
define annobuilder-template
mkdir -p $(ANNO_DATA)
$(if $(FASTA),
	$(eval override FASTA_PATH = $(shell readlink -f $(FASTA))),
	$(eval override FASTA_PATH=)
)
$(if $(FASTA_PATH),
	$(eval override FASTA_EXISTS = $(shell test -e $(FASTA_PATH) && echo exists || true)),
	$(eval override FASTA_EXISTS=)
)
$(if
	$(and $(FASTA_PATH),$(FASTA_EXISTS)),
	$(eval override ANNOBUILDER_OPTS += -v $(FASTA_PATH):/anno/data/FASTA/custom.fasta)
	$(eval override ANNOBUILDER_OPTS += -e FASTA=/anno/data/FASTA/custom.fasta)
)
docker run --rm $(TERM_OPTS) \
	$(ANNOBUILDER_OPTS) \
	-u "$(UID_GID)" \
	-v $(TMP_DIR):/tmp \
	-v $(ANNO_DATA):/anno/data \
	$(ANNOBUILDER_IMAGE_NAME) \
	bash $(BASH_I) -c "$(RUN_CMD) $(RUN_CMD_ARGS)"
endef

build-annobuilder: ## build Docker image of 'builder' target named ANNOBUILDER_IMAGE_NAME
	docker build -t $(ANNOBUILDER_IMAGE_NAME) $(BUILD_OPTS) --target builder .

build-all: build-annobuilder build-prod

pull-annobuilder: ## pull ANNOBUILDER_IMAGE_NAME from registry, requires USE_REGISTRY
	$(call check_defined, USE_REGISTRY, You must set USE_REGISTRY to pull remote images)
	docker pull $(ANNOBUILDER_IMAGE_NAME)

pull-all: pull-annobuilder pull-prod

# The annobuilder-exec/-help/-run/-shell targets are only run when troubleshooting data generation

annobuilder-exec: ## run a single command in ANNOBUILDER_CONTAINER_NAME
	@$(call check_defined, RUN_CMD, 'Use RUN_CMD="python3 <script> <args>" to define the command')
	$(annobuilder-template)

annobuilder-help: ## print usage instructions for the core builder script
	$(eval RUN_CMD := /anno/ops/sync_data.py --help)
	$(annobuilder-template)

annobuilder-run: ## run container ANNOBUILDER_CONTAINER_NAME based on image ANNOBUILDER_IMAGE_NAME
	docker run -dt \
		-v $(ANNO_DATA):/anno/data \
		--name $(ANNOBUILDER_CONTAINER_NAME) \
		--restart=always \
		$(ANNOBUILDER_IMAGE_NAME) \
		$(ANNOBUILDER_OPTS) \
		sleep infinity

annobuilder-shell: ## get a bash shell in ANNOBUILDER_CONTAINER_NAME
	docker exec -it $(ANNOBUILDER_CONTAINER_NAME) /bin/bash

show-data-size: ## print out the size of the datasets
	$(eval RUN_CMD := /anno/ops/get_sizes -s)
	$(annobuilder-template)

DB_CREDS_MSG := \
	Use DB_CREDS to specify the absolute path to a file containing definitions of the environment \
	variables SPACES_KEY and SPACES_SECRET necessary to upload data to Digital Ocean

download-data: ## download all datasets from DigitalOcean
	$(eval RUN_CMD := df -ih; df -h; python3 /anno/ops/sync_data.py --download)
	$(annobuilder-template)

download-package: ## download PKG_NAME data set from DigitalOcean
	@$(call check_defined, PKG_NAME, 'Use PKG_NAME to specify which package to download')
	$(eval RUN_CMD := python3 /anno/ops/sync_data.py --download -d $(PKG_NAME))
	$(annobuilder-template)

upload-data: ## upload all data sets to DigitalOcean using DB_CREDS credential file
	@$(call check_defined, DB_CREDS, $(DB_CREDS_MSG))
	$(eval RUN_CMD := python3 /anno/ops/sync_data.py --upload)
	$(annobuilder-template)

upload-package: ## upload PKG_NAME data set to DigitalOcean using DB_CREDS credential file
	@$(call check_defined, DB_CREDS, $(DB_CREDS_MSG))
	@$(call check_defined, PKG_NAME, 'Use PKG_NAME to specify which package to upload')
	$(eval RUN_CMD := python3 /anno/ops/sync_data.py --upload -d $(PKG_NAME))
	$(annobuilder-template)

generate-data: ## generate all data sets based on the version in datasets.json
	mkdir -p $(ANNO_RAWDATA)
	$(eval override ANNOBUILDER_OPTS += -v $(ANNO_RAWDATA):/anno/rawdata)
	$(eval RUN_CMD := python3 /anno/ops/sync_data.py --generate)
	$(annobuilder-template)

generate-package: ## generate PKG_NAME data set based on the version in datasets.json
	@$(call check_defined, PKG_NAME, 'Use PKG_NAME to specify which package to generate')
	mkdir -p $(ANNO_RAWDATA)
	$(eval override ANNOBUILDER_OPTS += -v $(ANNO_RAWDATA):/anno/rawdata)
	$(eval RUN_CMD := python3 /anno/ops/sync_data.py --generate -d $(PKG_NAME))
	$(annobuilder-template)

verify-digital-ocean:
	$(eval RUN_CMD := python3 /anno/ops/sync_data.py --verify-remote)
	$(annobuilder-template)

# NOTE: The installation of thirdparty packages is actually covered in the Dockerfile
#       This command is included here just in case
install-thirdparty-package: ## manually install thirdparty package PKG_NAME
	@$(call check_defined, PKG_NAME, 'Use PKG_NAME to specify which package to install')
	$(eval RUN_CMD := python3 /anno/ops/install_thirdparty.py --clean -p $(PKG_NAME))
	$(annobuilder-template)

# NOTE: The installation of thirdparty packages is actually covered in the Dockerfile
#       This command is included here just in case
install-all-thirdparty: ## manually install all thirdparty packages
	$(eval RUN_CMD := python3 /anno/ops/install_thirdparty.py --clean)
	$(annobuilder-template)

list-thirdparty: ## list all thirdparty packages available for installation
	$(eval RUN_CMD := python3 /anno/ops/install_thirdparty.py --list)
	$(annobuilder-template)

help-thirdparty: ## print usage instructions for the thirdparty software installer
	$(eval RUN_CMD := python3 /anno/ops/install_thirdparty.py --help)
	$(annobuilder-template)

tar-data: ## pack PKG_NAMES, or missing that, the entire content of ANNO_DATA into TAR_OUTPUT
	$(eval TAR_OUTPUT ?= data.tar)
	$(eval RUN_CMD := PKG_NAMES=$(PKG_NAMES) DATASETS=$(DATASETS) TAR_OUTPUT=$(TAR_OUTPUT) \
		/anno/ops/pack_data)
	$(annobuilder-template)

untar-data: ## unpack TAR_INPUT into ANNO_DATA, update sources.json, vcfanno_config.toml as needed
	$(eval TAR_INPUT ?= /anno/data/data.tar)
	$(eval RUN_CMD := TAR_INPUT=$(TAR_INPUT) /anno/ops/unpack_data)
	$(annobuilder-template)

# For consistency, the Docker container must be used when updating Pipfile dependencies. Otherwise,
# it will go off your local python's settings which may not match. This can happen even if using a
# Pipenv venv locally.
pipenv-update: ## updates Pipfile.lock using IMAGE_NAME, under development
	docker run --rm -it \
		-u anno-user \
		-v $(PWD):/local_anno \
		$(ANNOBUILDER_IMAGE_NAME) \
		/anno/ops/update_pipfile.sh

# Use the Annotation builder image to check dev and prod dependencies
pipenv-check: ## uses pipenv to check for package vulnerabilities
	$(eval RUN_CMD := pipenv check)
	$(annobuilder-template)


## -------------------------------------------------------------------------------------------------
## Singularity
##   Like the above sections, but using Singularity $SINGULARITY_IMAGE_NAME instance named
##   $SINGULARITY_INSTANCE_NAME.
##   Other vars: SINGULARITY_PG_UTA_DATA, ANNO_DATA, SINGULARITY_ANNO_LOGS
## -------------------------------------------------------------------------------------------------
.PHONY: singularity-build singularity-shell singularity-start singularity-stop singularity-test

# Create additional directories necessary when using read-only Singularity image
_ensure-singularity-dirs:
	@mkdir -p $(SINGULARITY_PG_UTA_DATA) $(SINGULARITY_ANNO_LOGS)

singularity-build:
	singularity build $(SINGULARITY_IMAGE_NAME) $(DOCKER_PREFIX)$(IMAGE_NAME)

singularity-shell: ## get a bash shell in SINGULARITY_INSTANCE_NAME
	singularity shell --cleanenv instance://$(SINGULARITY_INSTANCE_NAME)

singularity-start: _ensure-singularity-dirs ## \
## start a Singularity instance of SINGULARITY_IMAGE_NAME named SINGULARITY_INSTANCE_NAME
	singularity instance start \
		-B $(ANNO_DATA):/anno/data \
		-B $(SINGULARITY_ANNO_LOGS):/logs \
		-B $(SINGULARITY_PG_UTA_DATA):/pg_uta \
		-B $(shell mktemp -d):/anno/.cache \
		--cleanenv \
		$(SINGULARITY_IMAGE_NAME) $(SINGULARITY_INSTANCE_NAME)
	ln -sf $(SINGULARITY_LOG_STDOUT) $(SINGULARITY_ANNO_LOGS)/singularity.out
	ln -sf $(SINGULARITY_LOG_STDERR) $(SINGULARITY_ANNO_LOGS)/singularity.err

singularity-stop: ## stop the Singularity instance SINGULARITY_INSTANCE_NAME
	singularity exec --cleanenv instance://$(SINGULARITY_INSTANCE_NAME) \
		supervisorctl -c /anno/ops/supervisor.cfg stop all
	singularity instance stop $(SINGULARITY_INSTANCE_NAME)

singularity-tail-logs: ## tail logs from SINGULARITY_INSTANCE_NAME
	tail -f $(SINGULARITY_LOG_STDOUT) $(SINGULARITY_LOG_STDERR)

singularity-test: ## run tests in running SINGULARITY_INSTANCE_NAME
	-singularity exec --cleanenv instance://$(SINGULARITY_INSTANCE_NAME) \
		supervisorctl -c /anno/ops/supervisor.cfg stop all
	singularity exec --cleanenv instance://$(SINGULARITY_INSTANCE_NAME) \
		/anno/ops/run_tests.sh
	singularity exec --cleanenv instance://$(SINGULARITY_INSTANCE_NAME) \
		supervisorctl -c /anno/ops/supervisor.cfg start all

# Load the stderr log for $SINGULARITY_INSTANCE_NAME in $PAGER (default: less)
singularity-errlog:
	cat $(SINGULARITY_LOG_STDERR) | $(PAGER)

# Load the stdout log for $SINGULARITY_INSTANCE_NAME in $PAGER (default: less)
singularity-log:
	cat $(SINGULARITY_LOG_STDOUT) | $(PAGER)

singularity-start-dev:
	@mkdir -p $(SINGULARITY_PG_UTA_DATA)
	singularity -v instance start \
		-B $(ANNO_DATA):/anno/data \
		-B $(SINGULARITY_PG_UTA_DATA):/pg_uta \
		-B $(shell mktemp -d):/anno/.cache \
		--cleanenv \
		$(SINGULARITY_SANDBOX_PATH) $(SINGULARITY_INSTANCE_NAME)

singularity-stop-dev: singularity-stop

singularity-shell-dev: singularity-shell

singularity-test-dev: singularity-test

singularity-untar-data: ## untar TAR_INPUT
	$(eval TAR_INPUT ?= /anno/data/data.tar)
	singularity exec --cleanenv instance://$(SINGULARITY_INSTANCE_NAME) TAR_INPUT=$(TAR_INPUT) \
		/anno/ops/unpack_data

## -------------------------------------------------------------------------------------------------
## Releases
##   Create release artifacts locally and on Gitlab
##   Variables: RELEASE_TAG, IMAGE_NAME, SINGULARITY_IMAGE_NAME
## -------------------------------------------------------------------------------------------------
.PHONY: release

ci-build-devcontainer:
	$(MAKE) build-devcontainer \
		BUILD_OPTS="--cache-from=$(ANNOBUILDER_IMAGE_NAME) --cache-from=$(IMAGE_NAME)"

ci-build-docker:
	$(MAKE) build-release \
		BUILD_OPTS="--cache-from=$(IMAGE_NAME) --cache-from=$(ANNOBUILDER_IMAGE_NAME)"
	$(MAKE) build-annobuilder \
		BUILD_OPTS="--cache-from=$(ANNOBUILDER_IMAGE_NAME) --cache-from=$(IMAGE_NAME)"

ci-push-docker:
	docker push $(IMAGE_NAME)
	docker push $(ANNOBUILDER_IMAGE_NAME)

ci-release-init:
	apk add --update make git python3 py3-click docker-cli bash

ci-release: pull-prod release

check-release-tag:
	@$(call check_defined, RELEASE_TAG, 'Missing tag. Please provide a value on the command line')
	git rev-parse --verify "refs/tags/$(RELEASE_TAG)^{tag}"
	git ls-remote --exit-code --tags origin "refs/tags/$(RELEASE_TAG)"

# Skip tag validation if run in CI, ref .gitlab-ci.yml for use cases
release: $(if $(CI),,check-release-tag) singularity-build ## \
## build a release SINGULARITY_IMAGE_NAME for RELEASE_TAG based on IMAGE_NAME pulled from the \
## remote registry

