"""
This script receives a configuration json file and, optionally, an inputs
parser Python script which defines a "ParserInputs" class inheriting Pydantic
BaseSettings.

This script outputs a task specific configuration json file by matching
environment variable values to regexes defined in the configuration file.

If no input parser script is given, environmental variable values will be used
as they are.
"""

import os
import sys
import logging
import re
import json
from pathlib import Path
from typing import Any, Dict, List, Optional, Tuple
from pydantic import BaseModel, ConfigDict, FilePath, RootModel
from pydantic_settings import BaseSettings, SettingsConfigDict

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)

PARSED_CONFIG_FILE = 'task_config.json'


# config parser settings
class Settings(BaseSettings):
    """
    :CONFIG_PATH:   Required. Path to config file.
    :INPUT_SCHEMA:  Optional. Path to a python module defining a class named 'ParserInputs'
                              inheriting from `pydantic.BaseSettings`. Undefined attributes of
                              `pydantic.BaseSettings` instances are implicitly assigned default
                              values from any matching environment variables.
    """
    CONFIG_PATH: FilePath
    INPUT_SCHEMA: Optional[FilePath] = None
    model_config = SettingsConfigDict(case_sensitive=True, env_prefix='ANNO_')


# Schemas of global config
class GlobalConfigItem(BaseModel):
    comment: str
    regexes: Dict[str, str]
    config: Dict[str, Any]
    model_config = ConfigDict(extra="forbid")


class GlobalConfig(RootModel):
    root: List[GlobalConfigItem]

    def __iter__(self):
        return iter(self.root)

    def __getitem__(self, item):
        return self.root[item]

    def __len__(self) -> int:
        return len(self.root)


# Schemas of parsed config
class ParsedConfig(RootModel):
    root: Dict[str, Any]

    def __getitem__(self, item):
        return self.root[item]


# Parser
def parse_config(settings: Settings, environment: Dict[str, str]) -> Tuple[ParsedConfig, Dict[str, Any]]:
    global_config = []
    # load and validate global config
    with open(settings.CONFIG_PATH, 'r') as fh:
        global_config_obj = json.load(fh)
        global_config = GlobalConfig.model_validate(global_config_obj)

    used_inputs = {}
    accumulate_config = {}

    for subconfig in global_config:
        log.info(' CHECKING [ %s ] ...', subconfig.comment)
        for environment_key, regex in subconfig.regexes.items():
            if environment_key not in environment:
                raise RuntimeError(f'environment variable "{environment_key}" not set')
            env_value = environment[environment_key]
            used_inputs[environment_key] = env_value
            log.debug('checking %s="%s" against regex "%s"', environment_key, env_value, regex)
            if not re.match(regex, env_value):
                log.debug('not matched')
                log.info(' NOT ALL REGEXES MATCHED, SKIP [ %s ]', subconfig.comment)
                # only when all regexes match, is its config used
                break
            else:
                log.debug('matched')
        else:
            log.info(' ALL REGEXES MATCHED, UPDATE TASK CONFIG WITH:\n%s',
                     json.dumps(subconfig.config, indent=4))
            accumulate_config.update(subconfig.config)

    # validate parsed config
    parsed_config = ParsedConfig.model_validate(accumulate_config)

    return parsed_config, used_inputs


def main(settings, environment):
    (parsed_config, used_inputs) = parse_config(settings, environment)
    config_json = parsed_config.model_dump_json(indent=4)
    log.info('SUMMARY\nsettings:\n%s\nvariables used:\n%s\ntask config:\n%s',
             settings.model_dump_json(indent=4),
             json.dumps(used_inputs, indent=4),
             config_json)

    outfile = Path(PARSED_CONFIG_FILE)
    outfile.write_text(config_json)
    log.info('task config file generated at %s', outfile.absolute())


if __name__ == "__main__":
    # NOTE: plain `Settings` class instantiation results in fallback to matching environment
    #       variables (prefixed by `env_prefix`)
    settings = Settings()

    if settings.INPUT_SCHEMA:
        # collect and validate required environmental variables according to given schema
        import importlib.util
        schema = settings.INPUT_SCHEMA
        module_name = schema.stem + '_for_anno_parser'
        log.info(f"reading custom configuration module specifications from '{schema}'..")
        spec = importlib.util.spec_from_file_location(module_name, settings.INPUT_SCHEMA)
        log.info(f"creating custom configuration module '{module_name}'..")
        mod = importlib.util.module_from_spec(spec)
        sys.modules[module_name] = mod
        spec.loader.exec_module(mod)
        log.info("fetching custom configuration parser..")
        ParserInputs = getattr(mod, 'ParserInputs')

        log.info("setting custom configuration environment..")
        environment = ParserInputs().dict()
    else:
        # pass all environmental variables to parser; no validation
        environment = os.environ

    main(settings, environment)
