#!/bin/bash -e

SOURCE_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

usage="Usage: $(basename "$0")

	-h|--help                   print this help text
	--vcf <vcf>                 input VCF
	--hgvsc <hgvsc>             input HGVSC
	--regions <regions>         regions to slice input on
	--convert                   flag to run conversion only, not annotation
	-o|--outfolder <outfolder>  output folder (default: working directory)
	-p|--processes              number of cores to use for time-consuming steps (default number of cores available)

"

# Parse arguments
WORKDIR=${PWD}
CONVERT_ONLY=0
NUM_VEP_PROCESSES=${NUM_VEP_PROCESSES:-$(nproc)}
NUM_VCFANNO_PROCESSES=${NUM_VCFANNO_PROCESSES:-$(nproc)}
VEP_BUFFER_SIZE=${VEP_BUFFER_SIZE:-5000}
while [[ $# -gt 0 ]]; do
    case "$1" in
        --vcf)
            if [[ $2 = -* ]]; then
                echo "Need argument for $1"
                exit 1
            fi
            VCF="$2"
            shift
            ;;
        --hgvsc)
            if [[ $2 = -* ]]; then
                echo "Need argument for $1"
                exit 1
            fi
            HGVSC="$2"
            shift
            ;;
        --regions)
            if [[ $2 = -* ]]; then
                echo "Need argument for $1"
                exit 1
            fi
            REGIONS="$2"
            shift
            ;;
        --help | -h)
            echo "${usage}"
            exit 0
            ;;
        --outfolder | -o)
            if [[ $2 = -* ]]; then
                echo "Need argument for $1"
                exit 1
            fi
            WORKDIR="$2"
            shift
            ;;
        --convert)
            CONVERT_ONLY=1
            ;;
        --processes | -p)
            NUM_VEP_PROCESSES="$2"
            NUM_VCFANNO_PROCESSES="$2"
            shift
            ;;
        --buffersize)
            VEP_BUFFER_SIZE="$2"
            shift
            ;;

        *)
            echo "* Error: Invalid argument: $1"
            echo "${usage}"
            exit 1
            ;;

    esac
    shift
done

if [[ -n ${HGVSC} && -n ${VCF} ]]; then
    echo "Both HGVSC and VCF provided. Exiting."
    exit 1
fi

if [[ -z ${HGVSC} && -z ${VCF} ]]; then
    echo "Neither VCF or HGVSC provded. Exiting."
    exit 1
fi

if [[ -z ${FASTA} || -z ${ANNO} ]]; then
    echo "Missing one or more mandatory environment variables:"
    echo "FASTA: ${FASTA}"
    echo "ANNO: ${ANNO}"
    exit 1
fi

ANNO_DATA="${SOURCE_DIR}/../../data"
VCFANNO_CONFIG="${ANNO_DATA}/vcfanno_config.toml"

echo "ANNO version:"
cat "${SOURCE_DIR}/../../version"
echo ""
echo "Running annotation pipeline with: "
echo "VCF: ${VCF}"
echo "HGVSC: ${HGVSC}"
echo "REGIONS: ${REGIONS}"
echo "WORKDIR: ${WORKDIR}"
echo "CONVERT_ONLY: ${CONVERT_ONLY}"

# End parse arguments

cleanup() {
    EXIT_CODE=$?
    if ((EXIT_CODE != 0)); then
        handle_step_failed
    fi
    exit ${EXIT_CODE}
}

# Trap EXIT code to run cleanup function
trap cleanup EXIT

mkdir -p "${WORKDIR}"

### Reused functions

handle_step_done() {
    echo -e "$(date '+%Y-%m-%d %H:%M:%S.%N')\t${STEP}\tDONE" | tee -a "${STATUS_FILE}"
    touch "${OUTPUT_SUCCESS}"
    VCF=${OUTPUT_VCF}
}

handle_step_failed() {
    echo -e "$(date '+%Y-%m-%d %H:%M:%S.%N')\t${STEP}\tFAILED" | tee -a "${STATUS_FILE}"
    cp "${OUTPUT_LOG}" "${WORKDIR}/error.log"
    touch "${OUTPUT_FAILED}"
    echo "--FAILED"
    cat "${OUTPUT_LOG}"
}

handle_step_start() {
    STEP=$1
    WORKDIR_STEP="${WORKDIR}/${STEP}"
    mkdir -p "${WORKDIR_STEP}"
    OUTPUT_LOG="${WORKDIR_STEP}/output.log"
    OUTPUT_VCF="${WORKDIR_STEP}/output.vcf"
    OUTPUT_CMD="${WORKDIR_STEP}/cmd.sh"
    OUTPUT_SUCCESS="${WORKDIR_STEP}/SUCCESS"
    OUTPUT_FAILED="${WORKDIR_STEP}/FAILED"

    echo -e "$(date '+%Y-%m-%d %H:%M:%S.%N')\t${STEP}\tSTARTED" | tee -a "${STATUS_FILE}"
}

### End reused functions

### Set output folders

STATUS_FILE=${WORKDIR}"/STATUS"
echo -e "$(date '+%Y-%m-%d %H:%M:%S.%N')\tSTARTED\t" | tee -a "${STATUS_FILE}"

FINISH_FILE=${WORKDIR}"/FINISHED"
if [[ -f ${FINISH_FILE} ]]; then
    echo "Removing old finish file"
    rm "${FINISH_FILE}"
fi

FINAL_VCF=${WORKDIR}"/output.vcf"
if [[ -f ${FINAL_VCF} ]]; then
    echo "Removing old final vcf"
    rm "${FINAL_VCF}"
fi

### End set output folders

##################################
########### CONVERT ##############
##################################
if [[ -n ${HGVSC} ]]; then
    # Set environment variables for step
    handle_step_start "CONVERT"

    # Create and run command
    cmd="python3 ${ANNO}/src/conversion/convert.py ${HGVSC} ${OUTPUT_VCF} &> ${OUTPUT_LOG}"
    echo "${cmd}" >"${OUTPUT_CMD}"
    bash "${OUTPUT_CMD}"

    # Handle step exit
    handle_step_done
fi

# Store original VCF
# For use in targets
cp "${VCF}" "${WORKDIR}/original.vcf"

##################################
###### REMOVE STAR ALLELES #######
##################################
handle_step_start "REMOVE_STAR_ALLELES"

cmd="remove_star_alleles --input ${VCF} --output ${OUTPUT_VCF} &> ${OUTPUT_LOG}"
echo "${cmd}" >"${OUTPUT_CMD}"
bash "${OUTPUT_CMD}"

handle_step_done

##################################
######### VT DECOMPOSE ###########
##################################
# Set environment variables for step
handle_step_start "VT_DECOMPOSE"

# Fix wrong header for older GATK
sed -i 's/##FORMAT=<ID=AD,Number=\./##FORMAT=<ID=AD,Number=R/g' "${VCF}"

cmd="vt decompose -s -o ${OUTPUT_VCF} ${VCF} &> ${OUTPUT_LOG}"
echo "${cmd}" >"${OUTPUT_CMD}"
bash "${OUTPUT_CMD}"

handle_step_done

##################################
######### VT NORMALIZE ###########
##################################
handle_step_start "VT_NORMALIZE"

cmd="vt normalize -r ${FASTA} -o ${OUTPUT_VCF} ${VCF} &> ${OUTPUT_LOG}"
echo "${cmd}" >"${OUTPUT_CMD}"
bash "${OUTPUT_CMD}"

handle_step_done

##################################
############ VCFSORT #############
##################################
handle_step_start "VCFSORT"

cmd="vcf-sort -c ${VCF} | uniq > ${OUTPUT_VCF} 2> ${OUTPUT_LOG}"
echo "${cmd}" >"${OUTPUT_CMD}"
bash "${OUTPUT_CMD}"

handle_step_done

##################################
########### SLICING ##############
##################################
if [[ -n ${REGIONS} ]]; then
    # Set environment variables for step
    handle_step_start "SLICE"

    # Perform slicing
    cmd="bedtools intersect -header -wa -u -a ${VCF} -b ${REGIONS} > ${WORKDIR_STEP}/tmp_output.vcf 2> ${OUTPUT_LOG}"
    echo "${cmd}" >"${OUTPUT_CMD}"

    # Ensure that all multiallelic blocks are preserved
    # 1. Gather all multiallelic (whole or partial) blocks in a text file
    cmd="grep -oP '(?<=[\s;])OLD_MULTIALLELIC[^\s;]*' ${WORKDIR_STEP}/tmp_output.vcf | sort | uniq > ${WORKDIR_STEP}/tmp_multiallelic_blocks.txt 2> ${OUTPUT_LOG}"
    echo "${cmd}" >>"${OUTPUT_CMD}"

    # 2. Grep for these multiallelic sites in the $VCF from the previous step (whole blocks only)
    # Append these to the temporary file, re-sort and remove duplicates
    cmd="cat ${WORKDIR_STEP}/tmp_output.vcf <(grep -F -f ${WORKDIR_STEP}/tmp_multiallelic_blocks.txt ${VCF}) | vcf-sort -c | uniq > ${OUTPUT_VCF} 2> ${OUTPUT_LOG}"

    echo "${cmd}" >>"${OUTPUT_CMD}"
    bash "${OUTPUT_CMD}"

    rm "${WORKDIR_STEP}/tmp_multiallelic_blocks.txt" "${WORKDIR_STEP}/tmp_output.vcf"

    handle_step_done
fi

##################################
##### ANNOTATE WITH SEGDUPS  #####
##################################
handle_step_start "ANNOTATE_WITH_SEGDUPS"

cat >"${WORKDIR_STEP}/tmp_segdup_vcfannoconf.toml" <<EOL
[[annotation]]
file="$(find "${ANNO_DATA}"/SegDups/genomicSuperDups_*.bed.gz)"
columns=[7]
names=["__OVERLAP_SEGDUP"]
ops=["max"]
EOL

cmd="vcfanno ${WORKDIR_STEP}/tmp_segdup_vcfannoconf.toml ${VCF} > ${OUTPUT_VCF} 2> ${OUTPUT_LOG}"
echo "${cmd}" >"${OUTPUT_CMD}"
bash "${OUTPUT_CMD}"

rm "${WORKDIR_STEP}/tmp_segdup_vcfannoconf.toml"

handle_step_done

##################################
######## ADD SEGDUP TAGS #########
##################################
handle_step_start "ADD_SEGDUPS_TAGS"

# This is how the $VCF's ##INFO entry for segmental duplications overlaps is expected to read
OVERLAP_SEGDUP_INFO_REGEX='^##INFO=<ID=__OVERLAP_SEGDUP,Number=1,Type=Float,Description="calculated by max of overlapping'

# The ##INFO entry for segmental duplications overlaps is replaced with ##FILTER entries describing the different
# segmental duplications tags SEGDUP0to90, SEGDUP90to98, SEGDUP98to99 and SEGDUP99to100. The __OVERLAP_SEGDUP is
# subsequently replaced with the right SEGDUP tag, according to the floating point value of __OVERLAP_SEGDUP.
# Finally, for all sites with __OVERLAP_SEGDUP, PASS => SEGDUP, FAIL => SEGDUP;FAIL
awkcmd='BEGIN{
  FS = OFS = "\t"
  added_segdup_filter = 0
} {
  # replace _one_ segmental duplication "##INFO" entry with descriptive "##FILTER" entries of overlap bins
  if (!added_segdup_filter && $0 ~ overlap_segdup_info_regex) {
    print("##FILTER=<ID=SEGDUP0to90,Description=\"repeated region with less than 90% sequence identity to another\">")
    print("##FILTER=<ID=SEGDUP90to98,Description=\"repeated region with 90-98% sequence identity to another\">")
    print("##FILTER=<ID=SEGDUP98to99,Description=\"repeated region with 98-99% sequence identity to another\">")
    print("##FILTER=<ID=SEGDUP99to100,Description=\"repeated region with 99-100% sequence identity to another\">")
    added_segdup_filter=1
    next
  }
  # for each variant overlapping a segmental duplication, replace the __OVERLAP_SEGDUP tag with the right overlap bin
  if ($8 ~ "__OVERLAP_SEGDUP") {
    # initialize fracMatch
    fracMatch = 0
    # store the variant info field $8 (semicolon-separated) items in array infoArr
    split($8, infoArr, ";")
    # scan infoArr for __OVERLAP_SEGDUP items
    for (k in infoArr) {
      if (infoArr[k] ~ "^__OVERLAP_SEGDUP=") {
        # parse __OVERLAP_SEGDUP item to extract the overlap floating point value
        fracMatch = gensub("^__OVERLAP_SEGDUP=", "", "g",  infoArr[k])
        # erase parsed __OVERLAP_SEGDUP item from info field $8
        gsub(infoArr[k], "", $8)
      }
    }
    # bin segmental duplication overlap floating point values and assign the right tags
    segdupTag = "0to90"
    if (fracMatch > 0.90) segdupTag = "SEGDUP90to98"
    if (fracMatch > 0.98) segdupTag = "SEGDUP98to99"
    if (fracMatch > 0.99) segdupTag = "SEGDUP99to100"
    $7 ~ "PASS" ? $7 = segdupTag : $7 = segdupTag";"$7
  }
  print
}'

cmd="
awk -v overlap_segdup_info_regex='${OVERLAP_SEGDUP_INFO_REGEX}' \
    '${awkcmd}' ${VCF} > ${OUTPUT_VCF} 2> ${OUTPUT_LOG}
"

echo "${cmd}" >"${OUTPUT_CMD}"
bash "${OUTPUT_CMD}"

handle_step_done

##################################
######### GATK QUAL300  ##########
##################################
handle_step_start "GATK_QUAL300"

# For GATK QUAL < 300, PASS => QUAL300, FAIL => FAIL;QUAL300
awkcmd='BEGIN{
  FS = OFS = "\t"
  added_qual300_filter = 0
} {
  # add FILTER description to avoid warnings downstream
  # NOTE: this relies on there being a prior "##FILTER" entry or a header starting with "#CHROM"
  if (($0 ~ "^##FILTER=" || $0 ~ "^#CHROM") && !added_qual300_filter) {
    print("##FILTER=<ID=QUAL300,Description=\"GATK QUAL score lower than 300\">")
    added_qual300_filter = 1
  }
  if ($1 ~ /^#.*GATK/) is_GATK=1
  if ($1 !~ /^#/ && $6 < 300.0 && is_GATK) {
    $7 ~ "PASS" ? $7 = "QUAL300" : $7 = $7";QUAL300"
  }
  print
}'

cmd="awk '${awkcmd}' ${VCF} > ${OUTPUT_VCF} 2> ${OUTPUT_LOG}"

echo "${cmd}" >"${OUTPUT_CMD}"
bash "${OUTPUT_CMD}"

handle_step_done

##################################
########### VALIDATE #############
##################################
handle_step_start "VALIDATE"

cmd="validate_vcf --input ${VCF} --output ${OUTPUT_VCF} &> ${OUTPUT_LOG}"
echo "${cmd}" >"${OUTPUT_CMD}"
bash "${OUTPUT_CMD}"

handle_step_done

# Run annotation if not specified to run convert only
if [[ ${CONVERT_ONLY} = 0 ]]; then
    ##################################
    ############## VEP ###############
    ##################################
    handle_step_start "VEP"
    if [[ "$(grep -c '^#' "${VCF}")" -eq "$(grep -c '^.' "${VCF}")" ]]; then
        # HACK: If there are no variants in the vcf, VEP doesn't write anything..
        cmd="cp ${VCF} ${OUTPUT_VCF}"
    else
        cmd="vep_offline \
              --fasta ${FASTA} \
              --force_overwrite \
              --sift=b \
              --polyphen=b \
              --hgvs \
              --numbers \
              --domains \
              --regulatory \
              --canonical \
              --protein \
              --biotype \
              --pubmed \
              --symbol \
              --allow_non_variant \
              --fork=${NUM_VEP_PROCESSES} \
              --vcf \
              --allele_number \
              --no_escape \
              --failed=1 \
              --exclude_predicted \
              --hgvsg \
              --no_stats \
              --refseq \
              --buffer_size=${VEP_BUFFER_SIZE} \
              --custom ${ANNO_DATA}/RefSeq/GRCh37_refseq_$(jq -r '.refseq.version' "${ANNO_DATA}/sources.json")_VEP.gff.gz,RefSeq_gff,gff,overlap,1, \
              -i ${VCF} \
              -o ${OUTPUT_VCF} &> ${OUTPUT_LOG}"
    fi
    echo "${cmd}" >"${OUTPUT_CMD}"
    bash "${OUTPUT_CMD}"

    handle_step_done

    ##################################
    ############ VCFANNO #############
    ##################################
    handle_step_start "VCFANNO"

    cp "${VCFANNO_CONFIG}" "${WORKDIR_STEP}/vcfanno_config.toml"
    cmd="vcfanno -p ${NUM_VCFANNO_PROCESSES} -base-path ${ANNO_DATA} ${WORKDIR_STEP}/vcfanno_config.toml ${VCF} > ${OUTPUT_VCF} 2> ${OUTPUT_LOG}"
    echo "${cmd}" >"${OUTPUT_CMD}"
    bash "${OUTPUT_CMD}"

    handle_step_done
fi

# Create link to final vcf
ln -rs "${VCF}" "${FINAL_VCF}"
echo -e "$(date '+%Y-%m-%d %H:%M:%S.%N')\tFINALIZED\t" | tee -a "${STATUS_FILE}"
