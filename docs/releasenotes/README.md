---
title: Latest release
---

# Release notes: Latest releases

|Major versions|Minor versions|
|:--|:--|
| [v2.9](#version-29)| [v2.9.1](#version-291), [v2.9.1a](#version-291a), [v2.9.1b](#version-291b), [v2.9.1c](#version-291c), [v2.9.2](#version-292), [v2.9.2a](#version-292a), [v2.9.2b](#version-292b), [v2.9.2c](#version-292c), [v2.9.3](#version-293), [v2.9.3a](#version-293a), [v2.9.3b](#version-293b), [v2.9.3c](#version-293c), [v2.9.3d](#version-293d), [v2.9.4](#version-294) |
| [v2.8](#version-28)| [v2.8.0a](#version-280a), [v2.8.0b](#version-280b), [v2.8.1](#version-281), [v2.8.2](#version-282) |
| [v2.7](#version-27)| [v2.7.0a](#version-270a), [v2.7.1](#version-271), [v2.7.1a](#version-271a), [v2.7.2](#version-272) |
| [v2.6](#version-26)| [v2.6.0a](#version-260a), [v2.6.0b](#version-260b), [v2.6.0c](#version-260c), [v2.6.0d](#version-260d), [v2.6.0e](#version-260e), [v2.6.0f](#version-260f), [v2.6.0g](#version-260g) |
| [v2.5](#version-25)|
| [v2.4](#version-24)| [v2.4.1](#version-241) |


## Version 2.9.4

Release date: 17.02.2025

### Codebase
<!-- MR !224 -->
- Upgrade python from v3.9 to v3.12

## Version 2.9.3d

Release date: 10.02.2025

### Data sources
<!-- MR !222 -->
<!-- MR !220 -->
- VEP v113.3: add back merged cache


## Version 2.9.3c

Release date: 29.01.2025

### Data sources
<!-- MR !218 -->
- Clinvar 20250102

## Version 2.9.3b

Release date: 13.01.2025

### Data sources
<!-- MR !210 -->
- VEP v113.3

### Codebase
<!-- MR !216 -->
- Bugfix: Data packaging fails if the data set key contains hyphens

## Version 2.9.3a

Release date: 22.11.2024

### Data sources
<!-- MR !211 -->
- Clinvar update November 2024

## Version 2.9.3

Release date: 31.10.2024

### Codebase
<!-- MR !197 -->
- Update Pipenv and SeqRepo
<!-- MR !199 -->
- Refactor and document vars dataset property
<!-- MR !207 -->
- Move VCFANNO variables to Dockerfile
<!-- MR !209 -->
- Bug: fixed release notes typo

### Data sources
<!-- MR !206 -->
- Clinvar update October 2024

## Version 2.9.2c

Release date: 16.10.2024

### Data sources
<!-- MR !205 -->
- Clinvar update October 2024


## Version 2.9.2b

Release date: 17.09.2024

### Data sources
<!-- MR !201 -->
- Clinvar update September 2024

## Version 2.9.2a

Release date: 16.08.2024

### Data sources
<!-- MR !190 -->
- Clinvar update August 2024

## Version 2.9.2

Release date: 02.08.2024

### Codebase
<!-- MR !185 -->
- Investigate performance VEP

## Version 2.9.1c

Release date: 10.07.2024

### Data sources
<!-- MR !186 -->
- Clinvar update July 2024

## Version 2.9.1b

Release date: 25.06.2024

### Data sources
<!-- MR !182 -->
- Clinvar update June 2024

## Version 2.9.1a

Release date: 29.05.2024

### Data sources
<!-- MR !179 -->
- Clinvar update May 2024

## Version 2.9.1

Release date: 29.04.2024

### Codebase
<!-- MR !174 -->
- Bug: clinvar XML parser is still broken
<!-- MR !175 -->
- Remove rogue flag

### Data sources
<!-- MR !176 -->
- Clinvar update April 2024

## >> Version 2.9

Release date: 20.03.2024

### Codebase
<!-- MR !171 -->
- Bugfix: VEP110 does not annotate with MANE Select transcript (custom source)
<!-- MR !170 -->
- Bugfix: ClinVar updated their variation archive XML structure
<!-- MR !159 -->
- Add SpliceAI prediction annotation

## Version 2.8.2

Release date 29.02.2024

<!-- MR !166 -->
- Upgrade clinvar 01.02.2024
<!-- MR !152 -->
- Upgrade VEP 110.1
<!-- MR !164 -->
- Use RefSeq only for SNV annotation
<!-- MR !165 -->
- Remove VEP merged cache

## Version 2.8.1

Release date 07.02.2024

<!-- MR !161 -->
- Upgrade vcfanno to 0.3.5

## Version 2.8.0b

Release date: 22.12.2023

### Data sources
<!-- MR !155 -->
- Updated Clinvar to version 20231207.

## Version 2.8.0a

Release date: 24.11.2023

### Data sources
<!-- MR !151 -->
- Updated Clinvar to version 20231102.

## >> Version 2.8

Release date: 20.10.2023

### Data sources
<!-- MR !149 -->
- Updated Clinvar to version 20231005.
<!-- MR !147 -->
- Updated Clinvar to version 20230803.

## Version 2.7.2

Release date: 27.06.2023

### Data sources
<!-- MR !139 -->
- Add VEP RefSeq cache. This enables the VEP `--refseq` option.

## Version 2.7.1a

Release date: 02.06.2023

### Codebase
<!-- MR !134 -->
- Change license from MIT to GNU GPL v3 license.

### Data sources
<!-- MR !135 -->
- Updated Clinvar to version 20230504.
<!-- MR !136 -->
- Update RefSeq GFF to 20220307.

## Version 2.7.1

Release date: 03.05.2023

### Codebase

<!-- MR !131 -->
- Sync data improvements.

## Version 2.7.0a

Release date: 27.04.2023

### Codebase
<!-- MR !127 -->
- Release history documentation update.
<!-- MR !128 -->
- Remove unwanted variables.
<!-- MR !129 -->
- Test release_singularity.

### Data sources
<!-- MR !127 -->
- Updated Clinvar to version 20230406.

## >> Version 2.7

Release date: 23.03.2023

### Codebase

<!-- MR !125 -->
- Errors in configuration parsing are hard to trace.
<!-- MR !118 -->
- Update VEP to 108.2.
<!-- MR !123 -->
- Remove existing rawdata directories when sync_data.py called with `--force`.

### Data sources
<!-- MR !124 -->
- Updated Clinvar to version 20230302.

## Version 2.6.0g

Release date: 20.02.2023

### Data sources
<!-- MR !121 -->
- Updated Clinvar to version 20230202.

## Version 2.6.0f

Release date: 24.01.2023

### Codebase
<!-- MR !119 -->
- release history documentation update

### Data sources
<!-- MR !119 -->
- Updated Clinvar to version 20230105.

## Version 2.6.0e

Release date: 30.12.2022

### Codebase
<!-- MR !116 -->
- Release history documentation update
- Correct build command in docs

### Data sources
<!-- MR !116 -->
- Updated Clinvar to version 20221201.

## Version 2.6.0d

Release date: 25.11.2022

### Data sources
<!-- MR !114 -->
- Updated Clinvar to version 20221103.

## Version 2.6.0c

Release date: 18.10.2022

### Data sources
<!-- MR !111 -->
- Updated Clinvar to version 20221006.

## Version 2.6.0b

Release date: 04.10.2022

### Data sources
<!-- MR !109 -->
- Updated Clinvar to version 20220901.

## Version 2.6.0a

Release date: 18.08.2022

### Data sources
<!-- MR !106 -->
- Updated Clinvar to version 20220804.

## >> Version 2.6

Release date: not released

### Codebase
<!-- MR !102 -->
- Update the pipenv lock file

### Data sources
<!-- MR !103 -->
- Updated Clinvar to version 20220707.

## >> Version 2.5

Release date: 20.06.2022

### Codebase
<!-- MR !98 -->
- Show size of annotation data before downloading

### Data sources
<!-- MR !99 -->
- Updated Clinvar to version 20220505.

## Version 2.4.1

Release date: 25.04.2022

### Codebase
<!-- MR !94 -->
- Release history documentation update.

### Data sources
<!-- MR !96 -->
- Updated Clinvar to version 20220407.

## >> Version 2.4

Release date: 18.03.2022

### Codebase
<!-- MR !89 -->
- Generic anno config parser.

### Data sources
<!-- MR !92 -->
- Updated Clinvar to version 20220303.

---
See [older releases](/releasenotes/olderreleases.md) for earlier versions.
