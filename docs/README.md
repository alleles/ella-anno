---
sidebar: false
---

<div style="text-align: center;padding-bottom: 5%;">
	<div style="padding-top: 7%;">
		<img width="350px;" src="./logo/anno_logo_docs.svg">
	</div>
</div>

[![Latest Release](https://gitlab.com/alleles/ella-anno/-/badges/release.svg)](https://gitlab.com/alleles/ella-anno/-/releases)

These pages describe ELLA's annotation service, which is required by ELLA to import variants and analyses. For documentation about the ELLA application itself, see [allel.es/docs](http://allel.es/docs).

*Features:* 
- Annotates variants with prediction, population frequencies and external mutation databases (VEP, gnomAD and ClinVar by default).
- Converts manual import data, with support for HGVS, VCF and SeqPilot formats.
- Extracts PubMed IDs from ClinVar and (optional) HGMD data.

## Contents

- [Technical documentation](/technical/): 
	- [Setup](/technical/setup.md): Instructions for setting up the annotation service.
	- [Annotation](/technical/annotation.md): Details about annotation sources, and how to update and add your own. 
	- [System internals](/technical/sysinternals.md): Technical details and inner workings of the service. 
- [Release notes](/releasenotes/): Versions and changes.


## Support

For support and requests, contact [ella-support](ma&#105;lt&#111;&#58;&#101;%6&#67;la&#37;2&#68;s&#117;pport&#64;m&#101;&#100;i&#115;&#105;&#110;&#46;%75i%&#54;F&#46;n%&#54;F).


