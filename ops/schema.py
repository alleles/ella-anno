datasets_schema = {
    "title": "Sources",
    "description": "Schema for annotation `datasets.json` file.",
    "type": "object",
    "patternProperties": {
        "^[-a-z0-9_]+$": {
            "description": "The main data set JSON template",
            "type": "object",
            "properties": {
                "comment": {
                    "description": "Details about the data set not belonging in its description",
                    "type": "string",
                },
                "description": {
                    "description": "Short description of the data set",
                    "type": "string",
                },
                "destination": {
                    "description": "Where to store the data set (relative to the data directory)",
                    "type": "string",
                },
                "generate": {
                    "description": "Shell operations to perform to generate the data set",
                    "type": "array",
                    "items": {"type": "string"},
                },
                "retries": {
                    "description": "Number of times to attempt download (after the first)",
                    "type": "integer",
                },
                "vars": {
                    "description": "Additional variables accessible to the 'generate' commands",
                    "type": "object",
                    "patternProperties": {"^[a-z][-a-z0-9_]*$": {"type": ["number", "string"]}},
                },
                "vcfanno": {
                    "description": "Instructions for VCF annotation by VCFAnno",
                    "type": "array",
                    "items": {
                        "type": "object",
                        "additionalProperties": False,
                        "properties": {
                            "file": {"type": "string"},
                            "fields": {"type": "array", "items": {"type": "string"}},
                            "names": {"type": "array", "items": {"type": "string"}},
                            "ops": {"type": "array", "items": {"type": "string"}},
                        },
                        "required": ["file", "fields", "names", "ops"],
                    },
                },
                "version": {"type": "string"},
            },
            "required": ["description", "destination", "generate", "version"],
            "additionalProperties": False,
        }
    },
    "definitions": {},
}


thirdparty_packages_schema = {
    "title": "Third-party packages",
    "description": "Schema for third-party annotation software `thirdparty.json` file.",
    "type": "object",
    "patternProperties": {
        "^[-a-z0-9_]+$": {
            "type": "object",
            "properties": {
                "filename": {"description": "Name of the software release file", "type": "string"},
                "installation": {
                    "description": "Shell operations to perform to generate the data set",
                    "type": "array",
                    "items": {"type": "string"},
                },
                "sha256": {"description": "Hash value", "type": "string"},
                "src_dir": {
                    "description": "Directory used to store the installation files",
                    "type": "string",
                },
                "url": {"description": "URL of the software repository", "type": "string"},
                "url_prefix": {
                    "description": "Eventual prefix to interpose between 'url' and 'filename'",
                    "type": "string",
                },
                "version": {
                    "description": "The version corresponding to the software release",
                    "type": "string",
                },
            },
            "required": [
                "filename",
                "installation",
                "sha256",
                "src_dir",
                "url",
                "version",
            ],
        }
    },
}
