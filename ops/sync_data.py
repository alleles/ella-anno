#!/usr/bin/env python3

import argparse
import atexit
import datetime
import json
import logging
import os
import shutil
import subprocess
import time
from collections.abc import Mapping
from pathlib import Path
from typing import Any

import toml
import yaml
from data_spaces import DataManager

from schema import datasets_schema, thirdparty_packages_schema
from util import AnnoJSONEncoder, HashType, format_obj, hash_directory_async, validate_json

# set up logging before anything else touches it
log_format = "%(asctime)s - %(module)s - %(funcName)s:%(lineno)d - %(levelname)s - %(message)s"
logging.basicConfig(level=logging.INFO, format=log_format)
logger = logging.getLogger(__name__)
atexit.register(logging.shutdown)

this_dir = Path(__file__).parent.absolute()
root_dir = this_dir.parent
# check for ANNO_DATA env variable, otherwise use {root_dir}/data
default_data_dir = Path(os.getenv("ANNO_DATA", root_dir / "data"))
default_rawdata_dir = root_dir / "rawdata"
default_dataset_file = this_dir / "datasets.json"
default_spaces_config = this_dir / "spaces_config.json"
default_thirdparty_file = this_dir / "thirdparty.json"
# get available CPUs, in case of restricted run environment
default_max_processes = min(len(os.sched_getaffinity(0)), 20)
TOUCHFILE = "DATA_READY"


def main():
    """
    Generate, download or upload data sets based on the recipes provided via --dataset-file.

    The recipes provided are expected to adhere to the JSON schema:\n\n{schema}

    The file should include each data set's "description" and instructions to "generate" it.

    A data set's "version" and its "destination" within the annotation data directory, normally
    assumed to be '{data_directory}', are among the properties required for each data set. These
    will be accessible by the commands defined in the "generate" property.

    The "generate" commands are run from a subdirectory of the raw data directory, normally assumed
    to be '{rawdata_directory}', called like the data set's "destination" directory.

    Each command in the "generate" array will be run in a separate shell. Eventual shell variables
    defined within one command will not be accessible to any other, even if exported. Any variables
    required _across_ commands should be defined in the "vars" object property. They can then be
    accessed in command strings by enclosing them in curly {{}} braces.

    Other _magic_ variables available to the "generate" command strings are:

    root_dir      path to the repository's root directory
    data_dir      path to the annotation data directory

    Any instructions for VCFAnno ("vcfanno" entry) will too have access to all variables the
    "generate" commands have access to.
    """
    parser = argparse.ArgumentParser(
        description=main.__doc__.format(
            data_directory=default_data_dir,
            rawdata_directory=default_rawdata_dir,
            schema=json.dumps(datasets_schema, indent=4).strip("{}"),
        ),
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )
    action_args = parser.add_mutually_exclusive_group(required=True)
    action_args.add_argument(
        "--download",
        action="store_true",
        help="download processed data sets from remote",
    )
    action_args.add_argument(
        "--generate",
        action="store_true",
        help="generate processed data sets",
    )
    action_args.add_argument(
        "--upload",
        action="store_true",
        help="upload generated data to remote",
    )
    action_args.add_argument(
        "--verify-remote",
        action="store_true",
        dest="verify_remote",
        help="verify existence of pre-processed data sets",
    )
    parser.add_argument(
        "-f",
        "--dataset-file",
        type=Path,
        default=default_dataset_file,
        dest="dataset_file",
        help=f"JSON file containing data set info [Default: {default_dataset_file}]",
    )
    parser.add_argument(
        "-d",
        "--dataset",
        dest="dataset",
        help="restrict operations to a specific data set",
    )
    parser.add_argument(
        "--data-dir",
        metavar="/path/to/anno/data/dir",
        type=Path,
        default=default_data_dir,
        dest="data_dir",
        help=f"directory to write processed data to. Default: {default_data_dir}",
    )
    parser.add_argument(
        "--rawdata-dir",
        metavar="/path/to/anno/rawdata",
        type=Path,
        default=default_rawdata_dir,
        dest="rawdata_dir",
        help=f"directory to temporarily store unprocessed data in. Default: {default_rawdata_dir}",
    )
    parser.add_argument(
        "--thirdparty-file",
        type=Path,
        default=default_thirdparty_file,
        dest="thirdparty_file",
        help=f"JSON file with third-party software info [Default: {default_thirdparty_file}]",
    )
    parser.add_argument(
        "--max-processes",
        type=int,
        default=default_max_processes,
        dest="max_processes",
        help=f"max number of processes to run in parallel. Default: {default_max_processes}",
    )
    parser.add_argument(
        "--spaces-config",
        type=Path,
        default=default_spaces_config,
        dest="spaces_config",
        help=f"JSON config for spaces.DataManager. Default: {default_spaces_config}",
    )
    parser.add_argument(
        "--skip-validation",
        action="store_true",
        dest="skip_validation",
        help="skip md5 validation of downloaded files",
    )
    parser.add_argument(
        "--force",
        action="store_true",
        help="Overwrite existing data if versions do not match",
    )
    parser.add_argument(
        "--cleanup",
        action="store_true",
        help="clean up raw data after successful processing",
    )
    # TODO: add --validate, to check existing data/versions against sources.json, run at startup
    parser.add_argument("--verbose", action="store_true", help="be extra chatty")
    parser.add_argument("--debug", action="store_true", help="run in debug mode")
    args = parser.parse_args()

    h = logging.FileHandler(args.data_dir / "SYNC_DATA_LOG")
    h.setFormatter(logging.Formatter(log_format))
    logger.addHandler(h)

    # process args

    if args.debug:
        setattr(args, "verbose", True)

    if args.dataset_file.exists():
        datasets: dict[str, Any] = json.loads(args.dataset_file.read_text())
    else:
        raise IOError(f"Cannot read data set file: {args.dataset_file}")

    validate_json(datasets, datasets_schema)

    if args.dataset and args.dataset not in datasets.keys():
        raise ValueError(
            f"Invalid data set '{args.dataset}'. Must be one of: " +
            ", ".join(sorted(datasets.keys()))
        )

    if args.dataset:
        sync_datasets: dict[str, Any] = {args.dataset: datasets[args.dataset]}
    else:
        sync_datasets = datasets

    if args.spaces_config.exists():
        try:
            spaces_config: dict[str, Any] = json.loads(args.spaces_config.read_text())
        except Exception as e:
            logging.error(f"Failed to parse spaces config file: {args.spaces_config}")
            raise e
    else:
        raise FileNotFoundError(f"Specified --spaces-config {args.spaces_config} does not exist")
    spaces_config["skip_validation"] = args.skip_validation

    if args.thirdparty_file.exists():
        thirdparty_packages: dict[str, Any] = json.loads(args.thirdparty_file.read_text())
    else:
        raise IOError(f"Cannot read data set file: {args.dataset_file}")

    validate_json(thirdparty_packages, thirdparty_packages_schema)

    if args.generate:
        verb = "Generating"
    elif args.download:
        verb = "Downloading"
    elif args.upload:
        verb = "Uploading"
    else:
        verb = "Verifying"

    # now we actually start doing things

    sources_json_file: Path = args.data_dir / "sources.json"
    vcfanno_toml_file: Path = args.data_dir / "vcfanno_config.toml"

    errs = list()
    for dataset_name, dataset in sync_datasets.items():
        logger.info(f"{verb} data set {dataset_name}")
        dataset_dir: Path = args.data_dir.absolute() / dataset["destination"]
        rawdataset_dir: Path = args.rawdata_dir.absolute() / dataset["destination"]
        if dataset_name == "vep":
            # special treatment for VEP because its version depends on the software installation
            dataset_version: str = thirdparty_packages["vep"]["version"]
        else:
            # for everything else the version is extracted from the data sets file
            dataset_version = dataset.get("version", "")

        source_data = {"version": dataset_version}

        format_opts: dict[str, str] = {
            # directory paths, all absolute
            "root_dir": str(root_dir),
            "data_dir": str(args.data_dir.absolute()),
            # destination directory, relative to data_dir
            "destination": dataset["destination"],
            # version info
            "version": dataset_version,
        }
        if dataset.get("vars"):
            format_opts.update(dataset["vars"])

        bash_opts = ["pipefail", "errexit"]
        subp_env = os.environ.copy()
        subp_env["SHELLOPTS"] = ":".join([subp_env.get("SHELLOPTS", ""), *bash_opts]).lstrip(":")
        if args.generate:
            dataset_ready = dataset_dir / TOUCHFILE
            if dataset_ready.exists():
                dataset_metadata = load_yaml(dataset_ready)
                if str(dataset_metadata.get("version", "")) == str(dataset_version):
                    logger.info(f"Dataset {dataset_name} already complete, skipping\n")
                    continue
                else:
                    message = (
                        f"Found existing {dataset_name} version {dataset_metadata['version']}, "
                        f"but trying to generate version {dataset_version}"
                    )
                    if args.force:
                        logger.warning(
                            f"{message}. Deleting existing "
                            f"data directory '{dataset_dir.relative_to(root_dir)}' and "
                            f"raw data directory '{rawdataset_dir.relative_to(root_dir)}' "
                            "before continuing.."
                        )
                        shutil.rmtree(dataset_dir)
                        shutil.rmtree(rawdataset_dir, ignore_errors=True)
                    else:
                        raise RuntimeError(
                            f"{message}. Please delete directory "
                            f"'{dataset_dir.relative_to(root_dir)}' and try again."
                        )

            elif not dataset_dir.exists():
                dataset_dir.mkdir(parents=True)

            if not rawdataset_dir.exists():
                rawdataset_dir.mkdir(parents=True)

            assert (
                len(dataset["generate"]) > 0
            ), f"Empty generate list for {dataset_name} in {args.dataset_file}, nothing to do"
            for step_num, step in enumerate(dataset["generate"]):
                logger.debug(f"DEBUG - Step {step_num}: {step}\n")
                assert isinstance(step, str)
                step_str: str = format_obj(step, format_opts)

                # if the data set allows retries (e.g., Broad's crappy FTP server), retry until the
                # max is reached, then bail on error
                num_retries = 0
                max_retries = dataset.get("retries", 0)
                step_success = False
                while num_retries <= max_retries:
                    logger.info(f"Running: {step_str}  ### [Attempt #{num_retries+1}]")
                    step_resp = subprocess.run(
                        step_str,
                        cwd=rawdataset_dir,
                        env=subp_env,
                        executable="/bin/bash",
                        shell=True,
                        stderr=subprocess.PIPE,
                        stdout=h.stream,
                    )
                    if step_resp.returncode != 0:
                        msg_tup = tuple(msg for msg in step_resp.stderr.decode('utf-8').split('\n'))
                        errs.append(
                            (
                                dataset_name,
                                step_str,
                                f"Error(s) ({step_resp.returncode}):",
                            ) + msg_tup
                        )
                        if num_retries >= max_retries and max_retries > 0:
                            errs.append((dataset_name, "max retries exceeded without success"))
                            break
                        else:
                            num_retries += 1
                            time.sleep(1 * num_retries)
                    else:
                        step_success = True
                        break

                # if one step fails max retries, abort processing
                if step_success is False:
                    break

            # compute md5s for each file
            md5sum_file = dataset_dir / "MD5SUM"
            file_hashes = hash_directory_async(
                dataset_dir,
                hash_type=HashType.md5,
                ignore=[md5sum_file.name, dataset_ready.name],
            )
            with md5sum_file.open("wt") as md5_output:
                for file in sorted(file_hashes, key=lambda x: x.path):
                    print(f"{file.hash}\t{file.path}", file=md5_output)

            # only write if process finished successfully
            if step_success is True:  # type: ignore
                fin_time = datetime.datetime.utcnow()
                source_data["timestamp"] = fin_time  # type: ignore
                dump_yaml(source_data, dataset_ready)

            if args.cleanup:
                shutil.rmtree(rawdataset_dir)
        elif args.download or args.upload or args.verify_remote:
            mgr = DataManager(**spaces_config)
            cmd_args = [dataset_name, dataset_version, dataset_dir.relative_to(root_dir)]
            if args.download:
                should_download = True
                dataset_touchfile = dataset_dir / TOUCHFILE
                if dataset_touchfile.is_file():
                    dataset_metadata = load_yaml(dataset_touchfile)
                    if str(dataset_metadata["version"]) != str(dataset_version):
                        message = (
                            f"Found existing {dataset_name} version {dataset_metadata['version']}, "
                            f"but expected version {dataset_version}"
                        )
                        if args.force:
                            logger.warning(
                                f"{message}. Deleting existing "
                                f"data directory '{dataset_dir.relative_to(root_dir)}' and "
                                f"raw data directory '{rawdataset_dir.relative_to(root_dir)}' "
                                "before continuing.."
                            )
                            shutil.rmtree(dataset_dir)
                            shutil.rmtree(rawdataset_dir, ignore_errors=True)
                        else:
                            raise RuntimeError(
                                f"{message}. Please delete directory "
                                f"'{dataset_dir.relative_to(root_dir)}' and try again."
                            )
                    else:
                        should_download = False

                if should_download:
                    mgr.download_package(*cmd_args)

                dataset_metadata = load_yaml(dataset_touchfile)
                source_data["timestamp"] = dataset_metadata["timestamp"]

                if args.skip_validation:
                    logger.info(f"Skipping download validation for {dataset_name}")
                else:
                    logger.info("Validating downloaded data")
                    md5sum = dataset_dir / "MD5SUM"
                    if not md5sum.exists():
                        logger.error(
                            f"No MD5SUM file found for {dataset_name} at {md5sum}, the files can't "
                            "be validated"
                        )
                        continue
                    subprocess.run(
                        ["md5sum", "--quiet", "-c", "MD5SUM"], cwd=dataset_dir, check=True
                    )

                    logger.info(f"All {dataset_name} files validated successfully")
            elif args.upload:
                mgr.upload_package(*cmd_args)
            else:
                if not mgr.check_exists(*cmd_args):
                    raise RuntimeError(
                        f"Data for {dataset_name} version {dataset_version} incomplete or "
                        "non-existent on remote. Check requested/available versions."
                    )
                else:
                    logger.info(
                        f"Data for {dataset_name} version {dataset_version} available on remote."
                    )

        else:
            raise Exception("This should never happen, what did you do?!")

        if args.generate or args.download:
            if "vcfanno" in dataset:
                source_data["vcfanno"] = format_obj(dataset["vcfanno"], format_opts)
                update_vcfanno_toml(dataset_name, source_data["vcfanno"], vcfanno_toml_file)
            update_sources(sources_json_file, dataset_name, source_data)

    if errs:
        logger.error(f"Encountered errors with the following data sets:")
        for err_entry in errs:
            err_message = "\n=== ".join([str(x) for x in err_entry])
            logger.error(err_message)
            print(err_message)


def load_yaml(file: Path):
    return yaml.load(file.open("rt"), Loader=yaml.CLoader)


def dump_yaml(data: Mapping, file: Path):
    return yaml.dump(data, file.open("wt"), Dumper=yaml.CDumper)


def update_sources(sources_file, source_name, source_data):
    """Update sources.json file with data set metadata."""
    file_json = {}
    if sources_file.exists():
        file_json.update(json.loads(sources_file.read_text()))

    old_data = file_json.get(source_name, {})
    if source_data != old_data:
        file_json[source_name] = source_data
        sources_file.write_text(json.dumps(file_json, cls=AnnoJSONEncoder, indent=2) + "\n")
        logger.info(
            f"Updated {sources_file} for {source_name} "
            f"(version: {source_data.get('version', 'N/A')})"
        )
    else:
        logger.info(f"Not updating {sources_file} for {source_name}: data is unchanged")


def update_vcfanno_toml(dataset_name, vcfanno_entries, toml_file):
    """Update vcfanno_config.toml for the given data set."""
    toml_data = {}
    if toml_file.exists():
        toml_data.update(toml.loads(toml_file.read_text()))

    toml_data.setdefault("annotation", [])

    destinations = [Path(v["file"]).parent for v in vcfanno_entries]
    assert (
        len(set(destinations)) == 1
    ), f"Multiple destinations detected in vcfanno entries for {dataset_name}: {destinations}"
    destination = destinations[0]

    new_toml_data = {
        "annotation": [v for v in toml_data["annotation"] if Path(v["file"]).parent != destination]
    }

    update_files = []
    for i, anno_entry in enumerate(vcfanno_entries):
        new_toml_data["annotation"].append(anno_entry)
        update_files.append(anno_entry["file"])

    # rewrite toml file
    if list(sorted(new_toml_data["annotation"], key=lambda x: x["file"])) != list(
        sorted(toml_data["annotation"], key=lambda x: x["file"])
    ):
        logger.info(
            f"Creating new entry for {dataset_name} in {toml_file} with file(s) "
            ", ".join(update_files)
        )
        try:
            toml_file.write_text(toml.dumps(new_toml_data))
            logger.info(f"Updated {toml_file} successfully")
        except IOError as e:
            logger.error(f"Error writing to {toml_file}")
            raise e
    else:
        logger.info(
            f"Not updating {toml_file} for data set {dataset_name}; entry/entries unchanged."
        )


###


if __name__ == "__main__":
    main()
