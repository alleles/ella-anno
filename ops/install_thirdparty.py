#!/usr/bin/env python3

import argparse
import datetime
import json
import logging
import os
import re
import shutil
import subprocess
from collections.abc import Mapping
from pathlib import Path
from typing import Any, Union

from schema import thirdparty_packages_schema
from util import format_obj, hash_file, HashType, validate_json

# set up logging before anything else touches it
log_format = "%(asctime)s - %(module)s - %(funcName)s:%(lineno)d - %(levelname)s - %(message)s"
logging.basicConfig(level=logging.INFO, format=log_format)
logger = logging.getLogger(__name__)

this_dir = Path(__file__).parent.absolute()
root_dir = this_dir.parent
default_dir = root_dir / "thirdparty"
default_thirdparty_file = this_dir / "thirdparty.json"
# get available CPUs, in case of restricted run environment
default_max_processes = min(len(os.sched_getaffinity(0)), 20)
TOUCHFILE = "SETUP_COMPLETE"


def main():
    """
    Install third-party packages based on the recipes provided via --thirdparty-file.

    The recipes provided are expected to adhere to the JSON schema:\n\n{schema}

    The file should include each package's "description" and instructions for its "installation".

    The installation commands are run from the directory reserved to third-party software, normally
    assumed to be '{directory}', or a subdirectory thereof ("src_dir").

    Each command in the "installation" array will be run in a separate shell. Eventual shell
    variables defined within one command will not be accessible to any other, even if exported. Any
    variables required _across_ commands should be defined as separate properties in the same JSON
    file. They can then be accessed in command strings by enclosing them in curly {{}} braces.

    With the exception of "sha256", all properties can include any other properties in their
    definitions. In addition to the properties included in the JSON file itself, the definitions
    have also access to the runtime variable "max_procs" specified on the command line. this is
    useful if one wants to parallelize especially time-consuming installation steps.
    """
    parser = argparse.ArgumentParser(
        description=main.__doc__.format(
            schema=json.dumps(thirdparty_packages_schema, indent=4).strip("{}"),
            directory=default_dir,
        ),
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )
    parser.add_argument(
        "--directory",
        "-d",
        default=default_dir,
        dest="directory",
        help=f"directory to extract the third-party packages into. Default: {default_dir}",
    )
    parser.add_argument(
        "--thirdparty-file",
        type=Path,
        default=default_thirdparty_file,
        dest="thirdparty_file",
        help=f"JSON file with third-party software info [Default: {default_thirdparty_file}]",
    )
    parser.add_argument(
        "--package",
        "-p",
        metavar="PACKAGE_NAME",
        dest="package",
        help=f"install package with this name",
    )
    parser.add_argument(
        "--max-processes",
        "-x",
        type=int,
        default=default_max_processes,
        dest="max_processes",
        help="maximum number of processes to run in parallel",
    )
    parser.add_argument(
        "--list", action="store_true", dest="list", help="list available packages and exit"
    )
    parser.add_argument(
        "--clean", action="store_true", dest="clean", help="clean up intermediate files"
    )
    parser.add_argument("--verbose", action="store_true", dest="verbose", help="be extra chatty")
    parser.add_argument("--debug", action="store_true", dest="debug", help="run in debug mode")
    args = parser.parse_args()

    if args.debug:
        setattr(args, "verbose", True)
        logger.setLevel(logging.DEBUG)

    if args.thirdparty_file.exists():
        thirdparty_packages: dict[str, Any] = json.loads(args.thirdparty_file.read_text())
    else:
        raise IOError(f"Cannot read data set file: {args.thirdparty_file}")

    validate_json(thirdparty_packages, thirdparty_packages_schema)

    if args.list:
        print("The following packages are available for installation:")
        print(", ".join(thirdparty_packages.keys()))
        return

    # make sure third-party directory exists
    args.directory.mkdir(parents=True, exist_ok=True)

    if args.package and args.package in thirdparty_packages.keys():
        install_packages = {args.package: thirdparty_packages[args.package]}
    else:
        install_packages = thirdparty_packages

    for pkg_name, pkg in install_packages.items():
        # add args.max_processes to pkg so we can use a single dict with `format_obj()`
        pkg["max_procs"] = args.max_processes

        if pkg["src_dir"] == "":
            pkg_dir = args.directory
            final_dir = pkg_dir
        else:
            # the release version number must be resolved in the temporary package's installation
            # directory in case it is included in "src_dir", but removed from its final destination
            pkg_dir = args.directory / format_obj(pkg["src_dir"], pkg)
            final_dir = args.directory / format_obj(re.sub("-*{version}", "", pkg["src_dir"]), pkg)

        if args.verbose:
            logger.info(f"Using pkg_dir: {pkg_dir}")
            logger.info(f"Using final_dir: {final_dir}\n")

        pkg_touchfile = final_dir / TOUCHFILE
        # if src_dir is empty there won't be any temporary installation directory to delete
        if pkg["src_dir"] != "":
            if final_dir.exists():
                logger.debug(f"Found existing final_dir: {final_dir}")
                if pkg_touchfile.exists():
                    logger.info(
                        f"Package {pkg_name} is already installed on {pkg_touchfile.read_text()}"
                        "and will not be installed again"
                    )
                    continue
                else:
                    # assume failed install because no TOUCHFILE
                    shutil.rmtree(final_dir)
            elif pkg_dir.exists():
                # pkg_dir exists, but final_dir does not assume failed installation and remove
                shutil.rmtree(pkg_dir)

        if args.verbose:
            logger.info(f"Fetching {pkg_name}...\n")
        pkg_artifact = github_fetch_package(pkg, args.directory)

        if is_archive(pkg_artifact):
            if args.verbose:
                logger.info(f"Compiling / packaging {pkg_name}")
            subprocess.run(["tar", "xvf", pkg_artifact], cwd=args.directory, check=True)
        else:
            if args.verbose:
                logger.info(f"Not extracting non-archive artifact {pkg_artifact}")

        if args.clean and is_archive(pkg_artifact):
            pkg_artifact.unlink()

        for step_num, step in enumerate(pkg["installation"]):
            step_str = format_obj(step, pkg)
            logger.debug(f"DEBUG - Step {step_num}: {step}\n")
            step_resp = subprocess.run(step_str, shell=True, cwd=pkg_dir)
            if step_resp.returncode != 0:
                raise Exception(f"Error installing package {pkg_name} on step: {step_str}")

        if final_dir != pkg_dir:
            pkg_dir.rename(final_dir)

        # create a touchfile to mark that setup was successful, if src_dir available to write to
        if pkg["src_dir"]:
            pkg_touchfile.write_text(f"{datetime.datetime.utcnow()}")

        if args.debug:
            break


def github_fetch_package(pkg: Mapping, dest: Path, hash="sha256") -> Path:
    """downloads a release archive from github"""
    hash_type = HashType[hash]
    release_file = format_obj(pkg["filename"], pkg)
    release_filepath = dest / release_file
    if release_filepath.is_file():
        this_hash = hash_file(release_filepath, hash_type=hash_type)
        if this_hash == pkg[hash]:
            logger.info("Re-using existing package")
            return release_filepath
        else:
            logger.info("Removing partially downloaded package")
            release_filepath.unlink()

    url_prefix = pkg.get("url_prefix", "releases/download/v{version}")
    release_url = format_obj(f"{pkg['url']}/{url_prefix}", pkg)
    full_url = f"{release_url}/{release_file}"

    subprocess.run(["wget", full_url], cwd=dest, check=True)
    this_hash = hash_file(release_filepath, hash_type=hash_type)
    if this_hash != pkg[hash]:
        raise Exception(
            f"Checksum mismatch on {release_file}. Expected {pkg[hash]}, but got {this_hash}"
        )

    return release_filepath


def is_archive(filename: Union[str, Path]):
    return bool(re.search(r"\.tar\.(?:gz|bz2)$", f"{filename}"))


###


if __name__ == "__main__":
    main()
