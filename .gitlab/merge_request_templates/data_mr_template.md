## Description

**Dataset updated/created:**

**Dataset version:**

- [ ] Generated data has been validated (specify below)
- [ ] Versioning follows previous versioning pattern
- [ ] [Release Notes](https://gitlab.com/alleles/ella-anno/-/blob/dev/docs/releasenotes/README.md?ref_type=heads) are updated
- [ ] Data has been uploaded to DigitalOcean

## Notes to reviewer

## Validation of data

- [ ] Number of entries is reasonable
> You can check the numbers of entries with `zgrep -cv '^#' <repository>/data/variantdbs/<package>/hgmd_2024.2_norm.vcf.gz`
- [ ] File(s) is/are not truncated
> You can check if the file is truncated with `zcat <repository>/data/variantdbs/<package>/hgmd_2024.2_norm.vcf.gz | tail`
- [ ] File size(s) is/are reasonable

```
(put relevant validation data here, e.g. script output from $ANNO_DATA/SYNC_DATA_LOG here)
```
/label data::update