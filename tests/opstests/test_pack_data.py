# Test packing data sets

import os
from conftest import iterate_data_files, ANNO_DATA, tar_data_files


def check_tar_contents(filenames, data_tar_filenames, data_tar):
    assert set(filenames) - set(data_tar_filenames) == set(["vcfanno_config.toml"])
    assert set(data_tar_filenames) - set(filenames) == set(["PACKAGES"])

    for f in set(filenames) & set(data_tar_filenames):
        tar_f = data_tar.extractfile(f)
        with open(os.path.join(ANNO_DATA, f), "rb") as data_f:
            assert data_f.read() == tar_f.read()


def test_pack_data_all():
    "Test packaging all packages"
    data_tar = tar_data_files()
    data_tar_filenames = [name for name in data_tar.getnames() if data_tar.getmember(name).isfile()]
    filenames = [f for f in iterate_data_files()]

    check_tar_contents(filenames, data_tar_filenames, data_tar)
    assert (
        data_tar.extractfile("PACKAGES").read().decode("utf-8")
        == "clinvarmock\ndataset1\ndataset2\ndataset3\n"
    )
    data_tar.close()


def test_pack_iterations():
    "Test packaging one dataset at a time"
    datasets = []
    for dataset in ["dataset1", "dataset2"]:
        datasets.append(dataset)
        data_tar = tar_data_files(PKG_NAMES=[dataset])
        data_tar_filenames = [
            name for name in data_tar.getnames() if data_tar.getmember(name).isfile()
        ]
        filenames = [
            f
            for f in iterate_data_files()
            if any(ds in f.lower() for ds in datasets) or "/" not in f
        ]

        check_tar_contents(filenames, data_tar_filenames, data_tar)
        assert data_tar.extractfile("PACKAGES").read().decode("utf-8") == "\n".join(datasets) + "\n"
        data_tar.close()


def test_pack_single_package():
    "Test packaging single packages"
    for dataset in ["dataset1", "dataset2"]:
        data_tar = tar_data_files(PKG_NAMES=[dataset])
        data_tar_filenames = [
            name for name in data_tar.getnames() if data_tar.getmember(name).isfile()
        ]
        filenames = [f for f in iterate_data_files() if dataset in f.lower() or "/" not in f]
        check_tar_contents(filenames, data_tar_filenames, data_tar)
        assert data_tar.extractfile("PACKAGES").read().decode("utf-8") == "{}\n".format(dataset)
        data_tar.close()
        os.remove(data_tar.fileobj.name)
