#!/usr/bin/env bash

set -ETeu -o pipefail

dataset_dir=/anno/data/VEP

while getopts "d:" opt; do
    case "${opt}" in
        d)
            dataset_dir="${OPTARG}"
            ;;
        *)
            echo "Invalid flag: '${opt}'"
            exit 1
            ;;
    esac
done

shift $((OPTIND - 1))

optargs=("-a" "cf" "-c" "${dataset_dir}" "-l" "-n")

"$(dirname "$(realpath "$(which vep)")")"/INSTALL.pl "${optargs[@]}" -s homo_sapiens_merged -y GRCh37
"$(dirname "$(realpath "$(which vep)")")"/INSTALL.pl "${optargs[@]}" -s homo_sapiens_refseq -y GRCh37
