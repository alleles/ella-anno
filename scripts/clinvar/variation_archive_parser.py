import os
from collections import defaultdict
from lxml import etree

submitter_map = {
    "ARUP Institute,ARUP Laboratories": "ARUP",
    "Breast Cancer Information Core (BIC) (BRCA2)": "BIC (BRCA2)",
    "Breast Cancer Information Core (BIC) (BRCA1)": "BIC (BRCA1)",
    "Consortium of Investigators of Modifiers of BRCA1/2 (CIMBA), c/o University of Cambridge": "CIMBA",
    "Emory Genetics Laboratory,Emory University": "Emory Genetics Lab",
    "Evidence-based Network for the Interpretation of Germline Mutant Alleles (ENIGMA)": "ENIGMA",
    "Genetic Services Laboratory, University of Chicago": "Genetic Services Laboratory, Chicago",
    "Illumina Clinical Services Laboratory,Illumina": "Illumina",
    "LDLR-LOVD, British Heart Foundation": "LDLR-LOVD",
    "Laboratory for Molecular Medicine,Partners HealthCare Personalized Medicine": "Laboratory for Molecular Medicine",
    "Sharing Clinical Reports Project (SCRP)": "SCRP",
    "Tuberous sclerosis database (TSC2)": "TSC2",
}

record_xpath_wildcard = "./*[self::IncludedRecord | self::ClassifiedRecord]"
germline_xpath_wildcard = record_xpath_wildcard + "/Classifications/GermlineClassification"


def scalar_xpath(root, path, cast=None, require=False, **kwargs):
    v = root.xpath(path, **kwargs)
    assert len(v) <= 1, f"Multiple matches for XPath:'{path}' in:\n{etree.tostring(root).decode()}"
    if require:
        assert len(v) == 1, f"No matches for XPath:'{path}' in:\n{etree.tostring(root).decode()}"

    if len(v) == 0:
        return None
    else:
        v = v[0]
        if cast is not None:
            return cast(v)
        else:
            return v


class VariationArchiveParser:
    """
    Class for parsing required data from the ClinVar XML with tag <VariationArchive>.
    See https://www.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=clinvar&rettype=vcv&is_variationid&id=9 for example.
    """

    def __init__(self, root, archive_folder=None):
        assert root.tag == "VariationArchive"
        self.root = root
        self.archive_folder = archive_folder
        if self.archive_folder:
            self._write_archive()
        self.position_warnings = defaultdict(list)
        self.variation_warnings = []

    def is_germline(self):
        return scalar_xpath(self.root, germline_xpath_wildcard) is not None

    @property
    def variant_id(self):
        if not hasattr(self, "_variant_id"):
            self._variant_id = scalar_xpath(self.root, "@VariationID", require=True)
        return self._variant_id

    @property
    def variation_type(self):
        return scalar_xpath(self.root, "@VariationType", require=True)

    @property
    def alleles(self):
        return self.root.xpath("./InterpretedRecord/SimpleAllele")

    def _write_archive(self):
        "Write XML to archive <self.archive_folder>/<variant_id>[:4]/<variant_id>.xml"
        archive_subdir = os.path.join(self.archive_folder, self.variant_id[:4])
        if not os.path.isdir(archive_subdir):
            try:
                os.mkdir(archive_subdir)
            except OSError:
                assert os.path.isdir(archive_subdir)
        with open(os.path.join(archive_subdir, self.variant_id + ".xml"), "w") as f:
            f.write(etree.tostring(self.root, encoding="utf-8", pretty_print=False).decode())

    @property
    def positions(self):
        """
        Positions in the XML are normally defined with the attributes:
            - positionVCF
            - referenceAlleleVCF
            - alternateAlleleVCF
        Rather than attempting to fix the cases where this is not the case, we ignore them.
        Note that any variants included in the ClinVar VCF release will be recorded _with_ position.
        """
        alleles = self.alleles
        xml_positions = set()
        if not alleles:
            return xml_positions
        else:
            assert len(alleles) == 1
            allele = alleles[0]
            seq_locs = allele.xpath("./Location/SequenceLocation[@Assembly='GRCh37']")
            for seq_loc in seq_locs:
                chrom = scalar_xpath(seq_loc, "@Chr", smart_strings=False)
                position = scalar_xpath(seq_loc, "@positionVCF", smart_strings=False)
                ref = scalar_xpath(seq_loc, "@referenceAlleleVCF", smart_strings=False)
                alt = scalar_xpath(seq_loc, "@alternateAlleleVCF", smart_strings=False)

                # Not well defined
                if any([x is None for x in [chrom, position, ref, alt]]):
                    continue
                xml_positions.add((chrom, position, ref, alt))
        return xml_positions

    @property
    def review_status(self):
        return scalar_xpath(self.root, germline_xpath_wildcard + "/ReviewStatus/text()")

    @property
    def clinical_significance(self):
        # Not currently used in CLINVARJSON. TODO: Update schema in ELLA
        clinsig = scalar_xpath(self.root, germline_xpath_wildcard + "/Description/text()")
        # If it's an included record, there is no classification for the variant
        if clinsig == "no classification for the single variant":
            clinsig = "N/A"
        return clinsig

    @property
    def submissions(self):
        "Loop over submissions in the XML, and extract required information"
        rcvs = dict()
        submitters = self.root.xpath(
            ".//ClinicalAssertionList//*[ClinVarAccession[@Accession][@SubmitterName]]"
        )
        for scv in submitters:
            scv_id = scalar_xpath(scv, "./ClinVarAccession/@Accession", require=True)
            submitter_name = scalar_xpath(scv, "./ClinVarAccession/@SubmitterName", require=True)
            submitter_name = submitter_map.get(submitter_name, submitter_name).strip(".,;:")
            traitnames = scv.xpath("./TraitSet/Trait/Name/ElementValue/text()")
            scv_clinrevstat = scalar_xpath(
                scv, "./Classification/ReviewStatus/text()", require=True
            )
            scv_clinsig = scalar_xpath(scv, "./Classification/GermlineClassification/text()")
            if not scv_clinsig:
                scv_clinsig = "not provided"
            last_evaluated = scalar_xpath(scv, "./Classification/@DateLastEvaluated")
            if last_evaluated is None:
                last_evaluated = "N/A"

            # TODO: We use this terrible structure for now, to keep backward compatibility.
            # Change when annotation is versioned in ELLA
            rcvs[scv_id] = {
                "traitnames": traitnames,
                "last_evaluated": [last_evaluated],
                "submitter": [submitter_name],
                "clinical_significance_descr": [scv_clinsig],
                "clinical_significance_status": [scv_clinrevstat],
                "variant_id": [self.variant_id],
            }
        return rcvs

    @property
    def pubmed_ids(self):
        """
        Pubmed IDs shown on website.
        Refer to https://www.ncbi.nlm.nih.gov/clinvar/docs/variation_report/#citations:
            > Citations can be provided in several places in submissions; this table includes all
            > citations except for those submitted on the condition and citations submitted as
            > assertion criteria.
        """
        return list(
            set(
                self.root.xpath(
                    (
                        ".//Citation"
                        "[not(ancestor::Trait)]"
                        "[not(ancestor::AttributeSet/Attribute[@Type='AssertionMethod'])]"
                        "/ID[@Source='PubMed']/text()"
                    )
                )
            )
        )

    def parse(self):
        "Parse and return data dictionary"
        data = {
            "positions": self.positions,
            "clinsig": self.clinical_significance,
            "revstat": self.review_status,
            "variation_warnings": self.variation_warnings,
            "position_warnings": self.position_warnings,
            "variation_type": self.variation_type,
            "variant_id": self.variant_id,
            "pubmed_ids": self.pubmed_ids,
            "submissions": self.submissions,
        }

        return data
