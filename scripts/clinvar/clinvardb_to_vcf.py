#!/usr/bin/env python3
"""
Read ClinVar XML data and parse into vcf-file.

We wish to accurately represent the data available on the website. ClinVar provides multiple data sources for its data, but none of them seems to be complete.
For as complete solution as possible, we use the efetch utility from entrez (https://www.ncbi.nlm.nih.gov/books/NBK25499/#_chapter4_EFetch_). This gives, as far as I can tell,
the complete data for what's shown at the website, per variation.

From the XML results returned here, we attempt to calculate the VCF position(s) of each variation.
However, if the ClinVar vcf provides a different position for that variant, we fall back to this.

Other data sources evaluated:
# VCF incomplete (lacking ~600 variant that I could parse from ClinVarFullRelease.xml). In addition, no submitter data available.
# -- Example variant ids missing from vcf: 208614, 556489 (inserted alleles given in GRCh37), 598978, 217477
# ClinVarFullRelease.xml incomplete. Doesn't have aggregated review status, and this must be computed. However, some SCVs are not present in this (e.g. SCV000077247 for variation id 2300), which makes this impossible.
# variation_archive_xxxxxx.xml.gz incomplete. Also lacks e.g. SCV000077247 for variation id 2300

For each variant, dump json data (b16 encoded) under the INFO-column in the vcf in the
ID CLINVARJSON. The data is then stored in a dict as

    ["rcvs"][scv_id][metadata_tag]=value

Each variant can have multiple SCVs.
NOTE: We store it under "rcvs" for backward compatibility - however, we do NOT store the actual RCV. Sorry for the confusion.

Each position in the VCF should have only one variation id associated with it, however, in some edge cases, the same variation ID can have multiple positiions.

By default, the fetched data from the entrez API, is archived for possible later inspection, as this is not archived on the ClinVar ftp servers.

Requirements (Python packages):
- lxml
- jsonschema

Requirements (command line tools):
- vcftools (vcf-validator, vcf-sort)
- bgzip

"""

import argparse
import base64
import datetime
import http.client
import json
import jsonschema
import multiprocessing
import logging
import os
import re
import signal
import subprocess
import sys
import tarfile
import traceback
import urllib.request, urllib.error, urllib.parse
from collections import defaultdict
from io import BytesIO
from lxml import etree
from multiprocessing.pool import Pool

from variation_archive_parser import VariationArchiveParser
from utils import vcf_sort, vcf_validator, bgzip, tabix, open_file


class IncompatibleDataError(RuntimeError):
    pass


class TimeoutError(RuntimeError):
    pass


def timeout(*args):
    raise TimeoutError("Timed out")


# Register timeout to be executed if SIGALRM is triggered.
# This is used to timeout potentially hanging processes
signal.signal(signal.SIGALRM, timeout)

TODAY = datetime.datetime.today().strftime("%d/%m/%Y")
CLINVAR_ARCHIVE = f"clinvar_raw_{TODAY.replace('/', '-')}"

# API KEY for entrez utilities
# [See https://ncbiinsights.ncbi.nlm.nih.gov/2017/11/02/new-api-keys-for-the-e-utilities]
API_KEY = os.environ.get("ENTREZ_API_KEY")

# Max retries for each batch, and which errors should trigger a retry
MAX_RETRIES = 10
RETRY_ERRORS = [
    http.client.IncompleteRead,
    urllib.error.HTTPError,
    urllib.error.URLError,
    TimeoutError,
]

# Default number of parallel processes (in case of multiple batches)
DEFAULT_NUM_PROCESSES = min(len(os.sched_getaffinity(0)), 20)

# Schema used in ELLA for ClinVar annotation.
# The dictionary encoded in the field CLINVARJSON should adhere to this schema.
CLINVAR_V1_SCHEMA = {
    "definitions": {
        "rcv": {
            "$id": "#/definitions/rcv",
            "type": "object",
            "required": [
                "traitnames",
                "clinical_significance_descr",
                "variant_id",
                "submitter",
                "last_evaluated",
            ],
            "properties": {
                "traitnames": {"type": "array", "items": {"type": "string"}},
                "clinical_significance_descr": {"type": "array", "items": {"type": "string"}},
                "variant_id": {"type": "array", "items": {"type": "string"}},
                "submitter": {"type": "array", "items": {"type": "string"}},
            },
        }
    },
    "$schema": "http://json-schema.org/draft-07/schema#",
    "$id": "http://example.com/root.json",
    "type": "object",
    "required": ["variant_description", "variant_id", "rcvs"],
    "properties": {
        "variant_description": {"$id": "#/properties/variant_description", "type": "string"},
        "variant_id": {"$id": "#/properties/variant_id", "type": "integer"},
        "rcvs": {"type": "object", "patternProperties": {"": {"$ref": "#/definitions/rcv"}}},
    },
}

VCF_HEADER = [
    "##fileformat=VCFv4.2",
    "##fileDate=" + TODAY,
    '##ID=<Description="ClinVar Variation ID">',
    '##INFO=<ID=CLINREVSTAT,Number=.,Type=String,Description="ClinVar review status for the Variation ID">',
    '##INFO=<ID=CLINSIG,Number=.,Type=String,Description="Clinical significance for this single variant">',
    '##INFO=<ID=CLINVARJSON,Number=1,Type=String,Description="Base 16-encoded JSON representation of metadata associated with this variant. Read back as lambda x: json.loads(base64.b16decode(x))">',
    '##INFO=<ID=VARIATION_ID,Number=1,Type=String,Description="The ClinVar variation ID">',
    '##INFO=<ID=VARIATION_TYPE,Number=1,Type=String,Description="Reported variation type">',
    '##INFO=<ID=WARN_FAILED_TO_CALCULATE_POSITION,Number=0,Type=Flag,Description="Failed to calculate position from xml data, using ClinVar VCF position">',
    '##INFO=<ID=WARN_VARIANT_ID_NOT_IN_CLINVAR_VCF,Number=0,Type=Flag,Description="Variant id not in ClinVar vcf">',
    '##INFO=<ID=WARN_MULTIPLE_POSITIONS,Number=0,Type=Flag,Description="Variant id is associated with multiple positions">',
    '##INFO=<ID=WARN_CLINVAR_VCF_CLINSIG_REVSTAT_MISMATCH,Number=.,Type=String,Description="ClinVar VCF reports a different clinsig and/or revstat. Formatted as clinsig:revstat">',
    '##INFO=<ID=WARN_CALCULATED_POSITION_MISMATCH,Number=.,Type=String,Description="Calculated a position different than ClinVar vcf. Shows calculated positions as chr:pos:ref/alt. Used ClinVar VCF position instead">',
    "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO",
]


def _write(outputfile, data, mode="a"):
    num_lines = 0
    with open(outputfile, mode) as f:
        for line in data:
            if not line.endswith("\n"):
                line += "\n"
            logging.debug(f"Writing line '{line}' to '{outputfile}'.")
            f.write(line)
            num_lines += 1
    logging.info(f"Wrote {num_lines} lines to {outputfile}")


def _write_header(outputfile):
    # Write header of output file
    _write(outputfile, VCF_HEADER, mode="w")


def _convert_alts(original_positions):
    # Convert positions where alt is in ALT_TRANSLATIONS
    # e.g. where for key ("1", 123, "A", "Y"), drop position in favour of ("1", 123, "A", "C") and
    # ("1", 123, "A", "T")
    ALT_TRANSLATIONS = {
        "N": ["A", "C", "G", "T"],
        "B": ["C", "G", "T"],
        "D": ["A", "G", "T"],
        "H": ["A", "C", "T"],
        "V": ["A", "C", "G"],
        "R": ["A", "G"],
        "Y": ["C", "T"],
        "S": ["G", "C"],
        "W": ["A", "T"],
        "K": ["G", "T"],
        "M": ["A", "C"],
    }
    positions = set()
    for chrom, pos, ref, alt in original_positions:
        if alt not in ALT_TRANSLATIONS:
            positions.add((chrom, pos, ref, alt))
        else:
            converted_positions = set(
                [(chrom, pos, ref, new_alt) for new_alt in ALT_TRANSLATIONS[alt] if new_alt != ref]
            )
            positions |= converted_positions

    return positions


def clinvar_vcf_check(data, clinvar_vcf):
    "Check parsed data from VariationArchiveParser against parsed ClinVar VCF"

    variant_id = data["variant_id"]
    variation_type = data["variation_type"]
    xml_positions = _convert_alts(data["positions"])
    if variant_id in clinvar_vcf:
        vcf_positions = _convert_alts([clinvar_vcf[variant_id]["position"]])
    else:
        vcf_positions = set()

    # Determine which positions to use. In most cases, the XML positions will match those in the VCF
    if not vcf_positions and not xml_positions:
        raise IncompatibleDataError(
            f"No positions found for variant id {variant_id} (type: {variation_type})"
        )

    elif vcf_positions and not xml_positions:
        # Using vcf positions
        logging.info(
            f"Could not find position in XML. Using ClinVar vcf position for {variant_id}"
        )
        data["variation_warnings"].append("WARN_FAILED_TO_CALCULATE_POSITION")
        data["positions"] = vcf_positions

    elif xml_positions and not vcf_positions:
        # Using XML positions
        logging.info(f"Could not find position in VCF. Using XML position for {variant_id}")
        data["variation_warnings"].append("WARN_VARIANT_ID_NOT_IN_CLINVAR_VCF")
        data["positions"] = xml_positions

    elif vcf_positions != xml_positions:
        # Found data in both XML and VCF, but they do not match...
        logging.info(
            f"Position mismatch for variant id {variant_id}. Using ClinVar VCF position.\n"
            + f"-- Calculated: {xml_positions}\n"
            + f"-- ClinVar: {vcf_positions}\n"
        )
        data["variation_warnings"].append(
            "WARN_CALCULATED_POSITION_MISMATCH"
            + "="
            + ",".join(
                f"{chrom}:{position}:{ref}/{alt}" for chrom, position, ref, alt in xml_positions
            )
        )
        data["positions"] = vcf_positions

    else:
        # Assert that they are equal (most cases)
        assert vcf_positions and xml_positions and vcf_positions == xml_positions
        data["positions"] = xml_positions

    if len(data["positions"]) > 1:
        data["variation_warnings"].append("WARN_MULTIPLE_POSITIONS")

    # Skip haplotypes/compound heterozygous variations as these might consist of alleles that are
    # submitted as standalone variations. All haplotypes reported with a single allele will be kept.
    if len(data["positions"]) > 1 and variation_type in [
        "Haplotype",
        "Compound heterozygote",
        "Phase unknown",
        "Diplotype",
    ]:
        assert (
            variant_id not in clinvar_vcf
        ), f"Variant id {variant_id} is in ClinVar VCF, but has multiple positions in XML"
        raise IncompatibleDataError(
            f"Variant {variant_id} of type {variation_type} has {len(data['positions'])} positions."
        )

    # Check that XML clinsig and revstat match VCF
    if variant_id in clinvar_vcf and (
        clinvar_vcf[variant_id]["clinsig"],
        clinvar_vcf[variant_id]["revstat"],
    ) != (data["clinsig"], data["revstat"]):
        clinsig_revstat = (
            f"{clinvar_vcf[variant_id]['clinsig']}:{clinvar_vcf[variant_id]['revstat']}"
        )
        data["variation_warnings"].append(
            f"WARN_CLINVAR_VCF_CLINSIG_REVSTAT_MISMATCH={clinsig_revstat}"
        )
        logging.info(
            f"Clinical significance or review status mismatch for variant id {variant_id}:\n"
            + f"-- VCF: {clinsig_revstat} <> XML: {data['clinsig']}:{data['revstat']}"
        )

    return data


def get_archive_root(path_to_tar):
    return os.path.basename(path_to_tar).removesuffix(".tar.gz")


def get_vcf_lines(data):
    json_data = {
        "variant_id": int(data["variant_id"]),
        "rcvs": data["submissions"],
        "pubmed_ids": data["pubmed_ids"],
        "variant_description": data["revstat"],
    }
    jsonschema.validate(json_data, CLINVAR_V1_SCHEMA)
    b16_info = base64.b16encode(
        json.dumps(json_data, separators=(",", ":")).encode("utf-8")
    ).decode("utf-8")
    vcf_lines = []
    if not data["variation_warnings"]:
        vcf_variation_warnings = ""
    else:
        vcf_variation_warnings = ";" + ";".join(data["variation_warnings"])

    for chrom, position, ref, alt in data["positions"]:
        if ref == alt:
            # Skip non-variants
            logging.info(
                f"Variant {data['variant_id']} [{(chrom, position, ref, alt)}] is non-polymorphic"
            )
            continue

        elif not (set(ref).issubset(set("ACGT")) and set(alt).issubset(set("ACGT"))):
            # Skip variants where ref or alt is not well defined
            logging.info(
                f"Variant {data['variant_id']} [{(chrom, position, ref, alt)}] is ill-defined"
            )
            continue

        vcf_lines.append(
            (
                f"{chrom}\t{position}\t{data['variant_id']}\t{ref}\t{alt}\t5000\tPASS\t"
                f"VARIATION_TYPE={data['variation_type'].replace(' ', '_')};"
                f"VARIATION_ID={data['variant_id']};CLINSIG={data['clinsig'].replace(' ', '_')};"
                f"CLINREVSTAT={data['revstat'].replace(' ', '_')};"
                f"CLINVARJSON={b16_info}{vcf_variation_warnings}"
            )
        )
    return vcf_lines


def xml_to_vcf(xml, clinvar_vcf, archive_folder):
    """
    Read all VariationReport tags and attempt to create one or more VCF lines from them.
    Return all VCF lines extracted from xml.
    """
    tree = etree.iterparse(BytesIO(xml), tag="VariationArchive", events=["end"])

    vcf_lines = []

    for _, root in tree:
        data = dict()
        logging.debug(f"Parsing XML root:\n{root}")
        variant_archive = VariationArchiveParser(root, archive_folder)
        if variant_archive.is_germline():
            data = variant_archive.parse()
        else:
            logging.warning(f"Discarded probable non-germline variant {variant_archive.variant_id}")
            continue

        try:
            data = clinvar_vcf_check(data, clinvar_vcf)
        except IncompatibleDataError as e:
            logging.info(f"Incompatible data: {str(e)}")
            continue

        added_vcf_lines = get_vcf_lines(data)

        if not added_vcf_lines and data["variant_id"] in clinvar_vcf:
            logging.error(
                (
                    f"No VCF lines produced for variant {data['variant_id']} even though it is"
                    "included in ClinVar VCF"
                )
            )
        elif added_vcf_lines and data["variant_id"] not in clinvar_vcf:
            logging.info(
                f"Variant {data['variant_id']} not in ClinVar VCF, but included in output"
            )

        vcf_lines += added_vcf_lines

        root.clear()

    return vcf_lines


def entrez_fetch_variation_data(ids):
    url = "https://www.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi"
    data = "db=clinvar&rettype=vcv&is_variationid&id=" + ",".join(ids)
    if API_KEY is not None:
        data += f"&api_key={API_KEY}"
    r = urllib.request.urlopen(url, data=data.encode("utf-8"))
    xml = r.read()
    return xml


def read_from_archive(ids, archive_file):
    # Read xml from archive
    xml = b"<ClinVarResult-Set>\n"
    with tarfile.open(archive_file, "r") as archive:
        archive_root = get_archive_root(archive_file)
        for variant_id in ids:
            member = os.path.join(archive_root, variant_id[:4], variant_id + ".xml")
            logging.debug(f"Searching archive '{member}' for variant '{variant_id}'.")
            try:
                variant_xml = archive.extractfile(member)
                xml += variant_xml.read()
            except KeyError:
                continue
    xml += b"</ClinVarResult-Set>"
    return xml


def execute_batch(ids, clinvar_vcf, archive_folder, archive_file):
    """
    Wrapper for running a forked job. Used to get properly formatted exceptions from child process
    in main process.
    """
    # Errors that will trigger a retry

    n = 0
    # Set timeout of 60 seconds (after this, a TimeoutError will be raised)
    signal.alarm(60)
    while True:
        try:
            if archive_file:
                xml = read_from_archive(ids, archive_file)
            else:
                xml = entrez_fetch_variation_data(ids)
            logging.debug(f"Read XML entry:\n{xml}")
            return xml_to_vcf(xml, clinvar_vcf, archive_folder)
        except Exception as e:
            logging.error(f"Exception in {multiprocessing.current_process().name}: {str(e)}")
            if type(e) in RETRY_ERRORS and n < MAX_RETRIES:
                logging.warning(f"{str(e)}. Retrying. ({n} retries)")
                n += 1
                continue
            else:
                raise Exception("".join(traceback.format_exception(*sys.exc_info())))
        finally:
            # Reset alarm, so that no TimeoutError will be raised
            signal.alarm(0)


def parse_clinvar_file(vcf_file, archive_folder):
    """
    Fetch positions (chrom, pos, ref, alt) and (clinical significance, review status) from VCF.
    Return a dictionary:
    {
        variant_id:
            {
                "position": (chrom, pos, ref, alt),
                "clinsig": <clinical significance>,
                "revstat": <review status>
            }
    }
    """
    clinvar_vcf = dict()

    # Write ClinVar file to archive. If archive_folder not given, just dump output to /dev/null
    if not archive_folder:
        archive_vcf_file = os.devnull
    else:
        archive_vcf_file = os.path.join(archive_folder, "clinvar.vcf")

    with open_file(vcf_file) as f, open(archive_vcf_file, "wt") as archive_f:
        for l in f:
            archive_f.write(l)
            if l.startswith("#"):
                continue

            chrom, pos, variant_id, ref, alt, _, _, info = l.strip().split("\t")
            assert variant_id not in clinvar_vcf
            # Skip non-variants
            if alt == ".":
                continue

            try:
                revstat = re.match(r".*CLINREVSTAT=([^;]+).*", info).groups(1)[0].replace("_", " ")
            except AttributeError as e:
                revstat = "N/A"

            try:
                clinsig = re.match(r".*CLINSIG=([^;]+).*", info).groups(1)[0].replace("_", " ")
            except AttributeError as e:
                clinsig = "N/A"

            clinvar_vcf[variant_id] = {
                "position": (chrom, pos, ref, alt),
                "clinsig": clinsig,
                "revstat": revstat,
            }

    assert len(set([v["position"] for v in clinvar_vcf.values()])) == len(
        clinvar_vcf
    ), "Some positions in the ClinVar VCF have multiple variant ids associated with them"

    return clinvar_vcf


def print_summary(vcf_file, clinvar_vcf):
    "Print a summary of the generated file, and compare it to the NCBI provided ClinVar VCF"
    total_warning_count = 0
    variant_count = 0
    variant_ids = set()
    positions = defaultdict(int)
    warnings = dict()
    with open(vcf_file, "r") as f:
        for l in f:
            if l.startswith("##INFO"):
                if "WARN" in l:
                    warnings[re.findall(".*?(WARN[^,]*)", l)[0]] = 0
            if l.startswith("#"):
                continue
            for w in warnings:
                if w in l:
                    warnings[w] += 1
            if "WARN" in l:
                total_warning_count += 1
            chrom, pos, variant_id, ref, alt = l.split("\t")[:5]
            position = (chrom, pos, ref, alt)
            positions[position] += 1

            variant_count += 1
            variant_ids.add(variant_id)

    added_ids = set(clinvar_vcf.keys()) - set(variant_ids)
    missing_ids = set(variant_ids) - set(clinvar_vcf.keys())
    n_variant_ids = len(variant_ids)
    n_added_ids = len(added_ids)
    n_missing_ids = len(missing_ids)

    summary = [
        "Output summary: ",
        f"-- Number of variants: {variant_count}",
        f"-- Number of ClinVar variation ids: {n_variant_ids}",
        f"-- Number of duplicated positions: {len([v for v in positions.values() if v > 1])}",
        f"-- Number of variants in output not in official ClinVar VCF: {n_added_ids}",
        f"-- Variants in official ClinVar VCF not in output [{n_missing_ids}]: {missing_ids}",
    ]
    summary.append(f"-- Number of variants with warnings: {total_warning_count}")
    summary.extend(f"-- Number of variants with {w}: {c}" for w, c in warnings.items())
    logging.info("\n".join(summary))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-o",
        "--output",
        type=str,
        required=True,
        help="Output VCF-file to write ClinVar database to",
    )
    parser.add_argument(
        "--clinvar-vcf",
        type=str,
        default="ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/vcf_GRCh37/clinvar.vcf.gz",
        dest="clinvar_vcf_file",
        help="Input VCF-file (.vcf or .vcf.gz) holding ClinVar variants parsed to VCF (incomplete)",
    )
    parser.add_argument(
        "--batch-size",
        type=int,
        default=1000,
        dest="batch_size",
        help="Number of variant ids per batch [NOTE: if too high, the Entrez API calls can fail]",
    )
    parser.add_argument(
        "--num-processes",
        type=int,
        default=DEFAULT_NUM_PROCESSES,
        dest="num_processes",
        help=f"Number of processes to run in parallel [{DEFAULT_NUM_PROCESSES}]",
    )
    parser.add_argument(
        "--padding",
        type=int,
        default=50000,
        dest="padding",
        help=f"Search margin above the maximum variant id found in ClinVar's VCF",
    )
    parser.add_argument(
        "--archive",
        action="store_true",
        default=False,
        dest="write_archive",
        help=f"If set, all fetched XMLs and VCF will be archived in '{CLINVAR_ARCHIVE}'",
    )
    parser.add_argument(
        "--archive-file", type=str, dest="archive_file", help="Path to archive to read from"
    )
    parser.add_argument("--debug", default=False, action="store_true", help="Print debug messages")
    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    else:
        logging.basicConfig(stream=sys.stdout, level=logging.INFO)

    if API_KEY is None:
        logging.warning(
            "The Entrez API is not set. This will limit the queries to 3/sec (instead of 10/sec)"
        )

    # Check whether to read from archive or not
    archive_file = args.archive_file
    if archive_file:
        if args.write_archive:
            logging.info("Reading from archive: won't write back to it")
        args.write_archive = False
        with tarfile.open(archive_file, "r") as f:
            clinvar_vcf_file = os.path.join(get_archive_root(archive_file), "clinvar.vcf")
            f.extract(clinvar_vcf_file)
            logging.info("VCF extracted")
    else:
        clinvar_vcf_file = args.clinvar_vcf_file

    outputfile = args.output
    batch_size = args.batch_size
    num_processes = args.num_processes

    _write_header(outputfile)

    if args.write_archive:
        archive_folder = os.path.normpath(CLINVAR_ARCHIVE)
        if not os.path.isdir(archive_folder):
            os.mkdir(archive_folder)
        else:
            raise RuntimeError(f"Archive folder {archive_folder} exists. Aborting.")
    else:
        archive_folder = None

    clinvar_vcf = parse_clinvar_file(clinvar_vcf_file, archive_folder)

    # There seems to be no definitive source for variation ids for ClinVar but they are sequential.
    # Find the maximum id in the ClinVar VCF, add substantial padding to this, and query for all
    # variation ids from 0 to max_variant_id + padding. If ClinVar doesn't have an entry for a
    # certain variation id, it will not be returned in the results.
    max_variant_id = max(int(x) for x in clinvar_vcf)
    variant_ids = [str(i) for i in range(max_variant_id + args.padding)]

    num_batches = len(variant_ids) // batch_size + int(len(variant_ids) % batch_size >= 1)
    logging.info(f"Submitting {num_batches} jobs of length {batch_size}")

    # Submit jobs in batches of batch_size. If num_processes > 1, run them in parallel using a
    # multiprocessing Pool.
    # Pool for downloading xmls using NCBIs Entrez API in parallel.
    # Increasing the size of the Pool could hit the rate limit of the Entrez API.
    # Furthermore, this is not the bottleneck, as parsing the results in the main thread is the

    if num_processes == 1:
        batch_number = 0
        logging.info("Running in serial.")
        while variant_ids:
            batch_number += 1
            batch_variant_ids, variant_ids = (variant_ids[:batch_size], variant_ids[batch_size:])
            data = execute_batch(batch_variant_ids, clinvar_vcf, archive_folder, archive_file)
            _write(outputfile, data)
            logging.info(f"Batch {batch_number} of {num_batches} completed")
    else:
        WORKERPOOL = Pool(processes=num_processes)
        jobs = []
        batch_number = 0
        while variant_ids:
            batch_number += 1
            batch_variant_ids, variant_ids = (variant_ids[:batch_size], variant_ids[batch_size:])
            job = WORKERPOOL.apply_async(
                execute_batch,
                (batch_variant_ids, clinvar_vcf, archive_folder, archive_file),
                callback=lambda x: _write(outputfile, x),
            )
            jobs.append(job)

        WORKERPOOL.close()
        processed = set()

        for i, job in enumerate(jobs):
            try:
                data = job.get()
                logging.info(f"Batch {i + 1} of {num_batches} completed")
                processed.add(i)
            except Exception as e:
                WORKERPOOL.close()
                WORKERPOOL.terminate()
                logging.error(f"Job {i + 1} failed with exception: {e}")
                raise
        # This should be "instantaneous", since all jobs are processed. If it takes longer,
        # it indicates that some job(s) did not finish.
        assert processed == set(range(len(jobs))), "Not all jobs processed"
        signal.alarm(60)
        WORKERPOOL.join()
        signal.alarm(0)

    if args.write_archive:
        subprocess.call(f"tar -cf {archive_folder}.tar.gz {archive_folder}", shell=True)
        subprocess.call(f"rm -rf {archive_folder}", shell=True)

    # Postprocessing
    vcf_sort(outputfile)
    vcf_validator(outputfile)
    print_summary(outputfile, clinvar_vcf)

    bgzip(outputfile)
    tabix(outputfile + ".gz")

    logging.info("Completed")


if __name__ == "__main__":
    main()
