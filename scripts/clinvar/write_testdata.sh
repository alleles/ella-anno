#!/usr/bin/env bash

BASEDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")/.." && pwd)"
TESTDIR="${BASEDIR}/tests/opstests/fixtures/clinvar"

entrez_url=https://www.ncbi.nlm.nih.gov/entrez/eutils

if [[ "${#@}" -gt 0 ]]; then
    echo "Please provide at least one location for ClinVar XML and VCF dumps."
    exit 1
fi

n=1
# extract a random set of XMLs from one or more ClinVar dumps
for x in $(find "$@" -type f -name '*.xml' | shuf | head); do
    mkdir -p "${TESTDIR}/${n}000"
    cp "${x}" "${TESTDIR}/${n}000/${n}000.xml"
    n=$((n + 1))
done

# write down the ClinVar Ids for the random set of variants
grep -or 'VariationID=[^<> ]\+' "${TESTDIR}" \
    | cut -d '=' -f 2 | tr -d '"' | sort -u | tee /tmp/ids.txt

# extract any existing variants from the ClinVar VCF dump
bcftools view -i 'ID=@/tmp/ids.txt' "${BASEDIR}/rawdata/variantDBs/clinvar/clinvar.vcf.gz" \
    >"${TESTDIR}/clinvar.vcf"

# add a known non-germline variant to the random set (make sure its ID is _lower_ than any other)
wget -O- \
    "${entrez_url}/efetch.fcgi?db=clinvar&rettype=vcv&is_variationid&id=3024186" \
    | tail -n +3 | sed 's|^</\?ClinVarResult-Set>||g;' \
    >"${TESTDIR}/100/100.xml"

# extract the Id map from the random set of XMLs and write a command to convert the Ids
grep -or 'VariationID=[^<> ]\+' "${TESTDIR}" \
    | tr ':' '/' | cut -d '/' -f 6,8 | tee /tmp/ids-map.txt \
    | awk -F '/' -v testdir="${TESTDIR}" '{
    printf( "sed -i s/%s/VariationID=\"%s\"/g; %s/*/* \n", $2, $1, testdir )
  }' | sed "s/ -i / -i '/g; s/g; /g;' /g;" >/tmp/cmd.sh

# run the command
bash /tmp/cmd.sh

# convert the Ids in the test VCF, too
tr -d '[:alpha:]/"' </tmp/ids-map.txt | tr '=' '\t' \
    | awk -F $'\t' '{
    if (NR==FNR) {
      map[$2] = $1
    }
    else {
      OFS=FS
      if ($0 != "^#" && $3 in map)
        $3 = map[$3]
      print($0)
    }
  }' /dev/stdin "${TESTDIR}/clinvar.vcf" >tmp.vcf
mv tmp.vcf "${TESTDIR}/clinvar.vcf"

# tar the whole thing
cd "${TESTDIR}/.." || {
    echo "WARNING: could not change to test directory"
    exit 1
}
tar cvzf clinvar.tar.gz "$(basename "${TESTDIR}")"/[1-9]* "$(basename "${TESTDIR}")"/clinvar.vcf
