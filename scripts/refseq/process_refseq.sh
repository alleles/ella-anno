#!/usr/bin/env bash

set -ETeu -o pipefail

dataset_dir=/anno/data/RefSeq

while getopts "d:" opt; do
    case "${opt}" in
        d)
            dataset_dir="${OPTARG}"
            ;;
        *)
            echo "unknown flag: '${opt}'"
            exit 1
            ;;
    esac
done

shift $((OPTIND - 1))

declare refseq_file=$*
declare refseq_vep_file="${refseq_file%.gff.gz}"_VEP.gff.gz

if [[ -z "${refseq_file:-}" ]]; then
    echo "No target specified. Aborting."
    exit 1
fi

zgrep -v -e '^#' -e 'miRBase:' "${refseq_file}" | sort -k1,1 -k4,4n -k5,5n -t $'\t' | bgzip -c \
    >"${refseq_vep_file}"
tabix -p gff "${refseq_vep_file}"

mv "${refseq_vep_file}"* "${dataset_dir}"/
