#!/usr/bin/env bash

# download gnomAD from http://gnomad.broadinstitute.org/downloads
#
# gnomAD keeps changing file name formats. This version is configured for the 2.0.2 release.

set -ETeu -o pipefail

GSURL='gs://gcp-public-data--gnomad/release'

usage() {
    if [[ -n $1 ]]; then
        echo "$1"
    fi
    echo
    echo "USAGE:"
    echo "    $0 < -v GNOMAD_REVISION > [ -s/--skip-md5 ] "
    echo
    exit 1
}

log() {
    echo "[$$] $(date +%Y-%m-%d\ %H:%M:%S) - $1"
}

pcnt() {
    local CNT
    CNT=$(pgrep -P $$ | wc -l)
    echo $((CNT - 1))
}

rm_on_mismatch() {
    local local_file="$1"
    local remote_hash="$2"
    local local_hash
    local_hash=$(openssl dgst -md5 -binary "${local_file}" | openssl enc -base64)
    if [[ "${local_hash}" != "${remote_hash}" ]]; then
        rm "${local_file}" # (hash mismatch)
    else
        log "Skipping already downloaded file: $(basename "${local_file}")"
    fi
}

while [ $# -gt 0 ]; do
    key="$1"
    case "${key}" in
        -v | --version)
            VERSION="$2"
            shift 2
            ;;
        -s | --skip-md5)
            SKIP_MD5=1
            shift
            ;;
        -h | --help)
            usage
            ;;
        *)
            break
            ;;
    esac
done

if [[ -z "${VERSION}" ]]; then
    usage "Error! revision is missing, e.g. 2.0.2"
fi

GSUTIL=$(which gsutil 2>/dev/null || echo)
if [[ -z ${GSUTIL} ]]; then
    usage "Unable to find gsutil, make sure it is installed, in the PATH and try again"
fi
export HOME=/tmp

MAX_PCNT=${MAX_PCNT:-$(grep -c processor /proc/cpuinfo)}

mapfile -t GS_FILES < <(
    ${GSUTIL} ls \
        "${GSURL}/${VERSION}/vcf/exomes/gnomad.exomes.r${VERSION}.sites.*.vcf.bgz*" \
        "${GSURL}/${VERSION}/vcf/genomes/gnomad.genomes.r${VERSION}.sites.*.vcf.bgz*"
)

# iterate over the remote files and compare their sizes and md5 checksums (unless set to skip md5)
# to those of their existing local counterparts.
# * if both values match, keep the local file and do not download it anew
# * otherwise, delete the local file and download a new copy
for gs_file in "${GS_FILES[@]}"; do
    local_file="$(basename "${gs_file}")"
    if [[ -f ${local_file} ]]; then
        # compare file size for faster exclusion
        gs_file_size=$(${GSUTIL} du "${gs_file}" | cut -f1 -d' ')
        if [[ ${gs_file_size} -eq $(stat -c %s "${local_file}") ]]; then
            if [[ -z "${SKIP_MD5:-}" ]]; then
                # checksum is single thread and slow, so let's parallelize what we can but not kill the CPU
                while [[ $(pcnt) -ge ${MAX_PCNT} ]]; do
                    sleep 15
                done
                gs_file_hash=$(
                    ${GSUTIL} hash "${gs_file}" | grep '(md5)' | perl -lane 'print $F[-1]'
                )
                rm_on_mismatch "${local_file}" "${gs_file_hash}" &
            fi
        else
            rm "${local_file}" # (size mismatch)
        fi
    fi
done
wait

# find any surviving local files and remove them from the list of files to download
readarray -d '' LOCAL_FILES < <(find . -mindepth 1 -maxdepth 1 -printf '%f\0')
for local_file in "${LOCAL_FILES[@]}"; do
    for i in "${!GS_FILES[@]}"; do
        if [[ "$(basename "${GS_FILES[${i}]}")" == "${local_file}" ]]; then
            unset "GS_FILES[${i}]"
        fi
    done
done

# download all new files in a parallel manner
if [[ ${#GS_FILES[@]} -gt 0 ]]; then
    log "Downloading ${#GS_FILES[@]} gnomAD files"
    ${GSUTIL} -m cp "${GS_FILES[@]}" .
else
    echo "All gnomAD files already downloaded"
fi
