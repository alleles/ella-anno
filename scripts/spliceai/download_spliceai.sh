#!/usr/bin/env bash

set -ETeu -o pipefail

if [[ -z ${BASESPACE_ACCESS_TOKEN:-} ]]; then
    exec 1>&2
    echo -e "\nNo BaseSpace access token provided."
    echo -e "Please make sure one is stored in the environment variable BASESPACE_ACCESS_TOKEN."
    exit 1
fi

if [[ "$#" -eq 1 ]]; then
    echo "Please provide a filename file."
fi

if [[ "$#" -eq 0 ]]; then
    echo "Please provide a file id and a filename file (in this order)."
fi

file_id=$1
shift

filename_file=$1
shift

bs get file --format=csv --field=Name --id="${file_id}" >"${filename_file}"
bs download file --id="${file_id}"
