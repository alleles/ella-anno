#!/usr/bin/env bash

set -ETeu -o pipefail

dataset_dir=/anno/data/FASTA

while getopts "d:h:" opt; do
    case "${opt}" in
        d)
            dataset_dir="${OPTARG}"
            ;;
        h)
            hash="${OPTARG}"
            ;;
        *)
            echo "Invalid flag: '${opt}'"
            exit 1
            ;;
    esac
done

shift $((OPTIND - 1))

declare gref=$*

if [[ -z "${gref}" ]]; then
    echo "No target specified. Aborting."
    exit 1
fi

if [[ -z "${hash:-}" ]]; then
    echo "Please provide hash type and value [-h <hash-type>=<hash-value>]."
    exit 1
else
    IFS='=' read -r hash_type hash_value <<<"${hash}"
    if [[ -z "${hash_type}" || -z "${hash_value}" ]]; then
        echo "Please provide both hash type and hash value [-h <hash-type>=<hash-value>]."
        exit 1
    fi
fi

mkdir -p "${dataset_dir}"

wget -c ftp://gsapubftp-anonymous@ftp.broadinstitute.org/bundle/b37/"${gref}"*

cat "${gref}".dict.gz.md5 "${gref}".fasta.gz.md5 "${gref}".fasta.fai.gz.md5 \
    | awk '{ n = split($2, path_arr, "/"); print($1, path_arr[n]); }' | md5sum -c

echo "${hash_value} ${gref}.fasta.gz" | "${hash_type}"sum -c
gunzip "${gref}"*.gz
bgzip "${gref}".fasta -c >"${gref}".fasta.gz
bgzip -r "${gref}".fasta.gz
cp "${gref}".fasta.fai "${gref}".fasta.gz.fai
faidx -i chromsizes "${gref}".fasta -o "${gref}".chromsizes
rm "${gref}"*.md5
mv "${gref}"* "${dataset_dir}/"
