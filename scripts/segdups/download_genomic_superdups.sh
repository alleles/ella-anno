#!/usr/bin/env bash

set -ETeu -o pipefail

dataset_dir=/anno/data/SegDups

while getopts "d:v:" opt; do
    case "${opt}" in
        d)
            dataset_dir="${OPTARG}"
            ;;
        v)
            version="${OPTARG}"
            ;;
        *)
            echo "Invalid option: '${opt}'"
            exit 1
            ;;
    esac
done

shift $((OPTIND - 1))

declare genomic_superdups_url=$*

if [[ -z "${genomic_superdups_url:-}" ]]; then
    echo "No target specified. Aborting."
    exit 1
fi

filename="$(basename "${genomic_superdups_url}")"

wget --timestamping "${genomic_superdups_url}"

zcat "${filename}" | awk -F $'\t' '{
    OFS = FS
    gsub(i"chr", "", $2)
    print($2, $3, $4, $5, $6, $7, $27, $28)
  }' \
    | sort -k1,1 -k2,2n -k3,3n \
    | bgzip >"genomicSuperDups_${version}.bed.gz"
tabix -p bed "genomicSuperDups_${version}.bed.gz"

mv "genomicSuperDups_${version}.bed.gz"* "${dataset_dir}/"
